// let $ctrl;
class AuthServices {
  constructor($http, APP_CONFIG, localStorageService) {
    this.$http = $http;
    this.TOKEN_KEY = APP_CONFIG.AUTH.TOKEN_KEY;
    this.localStorageService = localStorageService;
  }

  isReturningUser() {
    return (this.localStorageService.get(this.TOKEN_KEY));
  }

  getToken(){
    return this.localStorageService.get(this.TOKEN_KEY);
  }

  setToken(token) {
    return this.localStorageService.set(this.TOKEN_KEY, token);
  }

  removeToken() {
    return this.localStorageService.remove(this.TOKEN_KEY);
  }
}
AuthServices.$inject = ["$http", "APP_CONFIG", "localStorageService"];

export { AuthServices };
