"use strict";
import angular from "angular";
import { AuthServices } from "./auth.services";

const AUTH_MODULE = angular
  .module("authModule", [])
  .service("authServices", AuthServices)

export { AUTH_MODULE };
