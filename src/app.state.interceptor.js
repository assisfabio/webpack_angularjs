"use strict";

StateInterceptor.$inject = ["$transitions", "$state", "$rootScope", "loginServices"];

function StateInterceptor ($transitions, $state, $rootScope, loginServices) {
  $transitions.onBefore({}, function(){
    console.log('Before State');
    /**
     * 1. Verificar Login
     */
    if (!loginServices.isLogged()) {
      loginServices.logoutClient();
    }
  });
  $transitions.onSuccess({}, function(){
    console.log('Success State')
    $rootScope.actualPage = $state.current.name;
  });
}

export { StateInterceptor };
