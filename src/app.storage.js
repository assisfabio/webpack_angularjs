"use strict";

function AppStorage (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('sur');
}
AppStorage.$inject = ["localStorageServiceProvider"];

export { AppStorage };
