'use strict';

import "./core/bootstrap";

describe('Project Quickstart -> ', function() {
  describe('Expected Definitions -> ', function() {
    it('Module Main',function(){
      expect(angular.module("sur")).toBeDefined();
    });

    it('Module Ui',function(){
      expect(angular.module("ui")).toBeDefined();
    });
  });
});