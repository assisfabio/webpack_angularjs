"use strict";

import { AppResolve } from './app.resolve';

function AppRouting($stateProvider) {
  $stateProvider.state({
    name: "root",
    abstract: true,
    template: '<ui-view/>',
    resolve: {
      app: AppResolve
    }
  });
}
AppRouting.$inject = [ "$stateProvider"];

export { AppRouting };
