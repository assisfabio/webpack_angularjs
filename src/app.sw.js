import runtime from "serviceworker-webpack-plugin/lib/runtime";
import registerEvents from "serviceworker-webpack-plugin/lib/browser/registerEvents";
import applyUpdate from "serviceworker-webpack-plugin/lib/browser/applyUpdate";

function RegisterServiceWork($log) {
  if ('serviceWorker' in navigator && window.location.protocol === 'https:') {
    const registration = runtime.register()

    registerEvents(registration, {
      onInstalled: () => {
        $log.info('onInstalled');
      },

      onUpdateReady: () => {
        $log.info('onUpdateReady');
        applyUpdate().then(() => {
          window.location.reload()
        });
      },

      onUpdating: () => {
        $log.warn('onUpdating');
      },

      onUpdateFailed: () => {
        $log.error('onUpdateFailed');
      },

      onUpdated: () => {
        $log.info('onUpdated');
      }
    });
  }
  else {
    $log.warn('serviceWorker not available');
  }
}
RegisterServiceWork.$inject = ["$log"];

export { RegisterServiceWork };