// bootstrap.js

"use strict";

import angular from "angular";
import { APP } from "../app";

angular.bootstrap(document, [APP.name], {
  strictDi: true
});
