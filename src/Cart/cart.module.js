"use strict";
import angular from "angular";
import { CartServices } from "./cart.services";
import { CartIconComponent } from "./cart-icon/cart-icon.component";

const CART_MODULE = angular
  .module("cartModule", [])
  .service("cartServices", CartServices)
  .component('f2pCartIcon', new CartIconComponent());

export { CART_MODULE };
