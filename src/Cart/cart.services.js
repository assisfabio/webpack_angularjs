class CartServices {
  constructor($http, $rootScope, $log) {
    this.$http = $http;
    this.$rootScope = $rootScope;
    this.$log = $log;
  }

  getCart() {
    return this.$rootScope.cart;
  }

  setItem(item) {
    if (!this.$rootScope.cart) {
      this.$rootScope.cart = [];
    }
    this.$rootScope.cart.push(item);
    this.$rootScope.$broadcast('update-cart');
  }
}
CartServices.$inject = ["$http", "$rootScope", "$log"];

export { CartServices };
