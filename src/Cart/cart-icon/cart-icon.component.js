import "./cart-icon.scss";

class CartIconController {
  constructor($rootScope, cartServices) {
    this.$rootScope = $rootScope;
    this.cartServices = cartServices;
  }

  $onInit(){
      this.init();

      this.$rootScope.$on('update-cart', () => {
        this.init();
      });
  }

  init() {
    this.cart = this.cartServices.getCart();
  }
}
CartIconController.$inject = ["$rootScope", "cartServices"];

class CartIconComponent {
  constructor() {
    this.template = require("./cart-icon.view.html");
    this.controller = CartIconController;
    this.bindings = {
    };
  }
}

export { CartIconComponent };
