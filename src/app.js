/*global TESTE_VAR*/
"use strict";
// Tools
import angular from "angular";
import "angular-sanitize";
import "angular-local-storage";
import "angular-messages";

// 3rd Party Tools/Libs
import "@uirouter/angularjs";
import "oclazyload";
import 'popper.js';
import 'bootstrap';

import './UI/commons/ng-croppie/ng-croppie';
// Cercred Libs
import "./UI/ui.module";

// Cercred Modules
import "./Auth/auth.module";
import "./Cart/cart.module";
import "./Login/login.module";

import "./pages/pages.module";

import { AppStorage } from "./app.storage";
import { RegisterServiceWork } from "./app.sw";
import { StateInterceptor } from "./app.state.interceptor";
import { AppRouting } from "./app.routing";
import "./app.style.scss";

import { appConfig } from "../app.config"; // FIXME Need Remove this constant

import { AppConfig } from "./app.config";

const APP =  angular
  .module("sur", [
    // TOOLS
    "ui.router",
    "oc.lazyLoad",
    "LocalStorageModule",
    "ngMessages",
    "ngSanitize",

    // LIBRARIES
    "ui",

    // MODULES
    "authModule",
    "cartModule",
    "loginModule",
    "pagesModule"
  ])
  .constant("APP_CONFIG", appConfig)
  .constant('IS_MOBILE', /iphone|ipad|ipod|android/i.test(navigator.userAgent.toLowerCase()))
  .config(AppRouting)
  .config(AppStorage)
  .provider('$surProvider', SurProvider)
  .run(AppConfig)
  .run(RegisterServiceWork)
  .run(StateInterceptor);
 
  function SurProvider () {
    this.$get = function ($http) {
      return {
        configuration: function() {
          /**
           * FIXME
           * 1. Mudar AppConfig para .env - Ver TODO em README.MD
           * 2. Esse provider deve receber as configurações da Aplicação
           */
          console.log(`${TESTE_VAR}`); // Recebendo variavel de ambiente
          return $http({
            method: appConfig.endpoints.cadeiras.getData.method,
            url: appConfig.endpoints.cadeiras.getData.url
          });
        }
      };
    }
    this.$get.$inject  = ["$http"];
  }
export { APP };
