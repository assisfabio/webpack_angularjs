"use strict";

import angular from "angular";
import './account/account.module';
import './atendimento/atendimento.module';
import './cadeiras/cadeiras.module';
import './checkin/checkin.module';
import './jogos/jogos.module';
import './financeiro/financeiro.module';
import './loja-de-vantagens/loja-de-vantagens.module';
import './programa-de-beneficios/programa-de-beneficios.module';
import './servicos/servicos.module';
import './home/home.module';
import { PagesRouting } from "./pages.routing";

const PAGES_MODULE = angular
  .module("pagesModule", [
    'accountModule',
    'atendimentoModule',
    'cadeirasModule',
    'checkinModule',
    'financeiroModule',
    'jogosModule',
    'lojaDeVantagensModule',
    'programaDeBeneficiosModule',
    'servicosModule',
    'homeModule'
  ])
  .config(PagesRouting);

export { PAGES_MODULE };
