import angular from "angular";
import { CadeirasRouting } from "./cadeiras.routing";
import { CadeirasService } from "./cadeiras.services";

const CADEIRAS_MODULE = angular
  .module("cadeirasModule", [])
  .config(CadeirasRouting)
  .service('cadeirasService', CadeirasService);

export { CADEIRAS_MODULE };
