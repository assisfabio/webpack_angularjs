const CadeirasState = {
    name: "cadeiras",
    parent: 'root',
    url: "/cadeiras",
    component: "indexCadeirasComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "cadeiras.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_CADEIRAS_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { CadeirasState };