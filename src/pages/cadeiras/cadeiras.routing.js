"use strict";
import { CadeirasState } from "./cadeiras.states";

function CadeirasRouting($stateProvider) {
  $stateProvider.state(CadeirasState);
}
CadeirasRouting.$inject = [ "$stateProvider"];

export { CadeirasRouting };
