"use strict";

import angular from "angular";

class IndexCadeirasController {
  constructor($rootScope, loginServices, cadeirasService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.cadeirasService = cadeirasService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.cadeirasService.getData(this.type)
      .then((data) => {
        this.itens = data;
      });
  }
}
IndexCadeirasController.$inject = ["$rootScope", "loginServices", "cadeirasService", "$log"];

class IndexCadeirasComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexCadeirasController;
  }
}

const INDEX_CADEIRAS_MODULE = angular
  .module("cadeiras.index", [])
  .component("indexCadeirasComponent", new IndexCadeirasComponent());

export { INDEX_CADEIRAS_MODULE };
