let $ctrl;
class CadeirasService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData(data) {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.cadeiras.getData.method,
      url: $ctrl.appConfig.endpoints.cadeiras.getData.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }
}

CadeirasService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { CadeirasService };
