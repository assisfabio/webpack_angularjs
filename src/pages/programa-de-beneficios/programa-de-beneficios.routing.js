"use strict";
import { ProgramaDeBeneficiosState } from "./programa-de-beneficios.states";

function ProgramaDeBeneficiosRouting($stateProvider) {
  $stateProvider.state(ProgramaDeBeneficiosState);
}
ProgramaDeBeneficiosRouting.$inject = [ "$stateProvider"];

export { ProgramaDeBeneficiosRouting };
