let $ctrl;
class ProgramaDeBeneficiosService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData(data) {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.beneficios.getData.method,
      url: $ctrl.appConfig.endpoints.beneficios.getData.url,
      data: data
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }

  getCities() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.beneficios.getCities.method,
      url: $ctrl.appConfig.endpoints.beneficios.getCities.url
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }

  getCategories() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.beneficios.getCategories.method,
      url: $ctrl.appConfig.endpoints.beneficios.getCategories.url
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }
}

ProgramaDeBeneficiosService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { ProgramaDeBeneficiosService };
