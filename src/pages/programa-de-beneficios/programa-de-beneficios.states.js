const ProgramaDeBeneficiosState = {
    name: "programa-de-beneficios",
    parent: 'root',
    url: "/programa-de-beneficios",
    component: "indexProgramaDeBeneficiosComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "programadebeneficios.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_PROGRAMADEBENEFICIOS_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { ProgramaDeBeneficiosState };