import angular from "angular";
import { ProgramaDeBeneficiosRouting } from "./programa-de-beneficios.routing";
import { ProgramaDeBeneficiosService } from "./programa-de-beneficios.services";

const PROGRAMADEBENEFICIOS_MODULE = angular
  .module("programaDeBeneficiosModule", [])
  .config(ProgramaDeBeneficiosRouting)
  .service('programaDeBeneficiosService', ProgramaDeBeneficiosService);

export { PROGRAMADEBENEFICIOS_MODULE };
