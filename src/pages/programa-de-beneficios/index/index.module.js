"use strict";

import angular from "angular";
import './index.scss';

class IndexProgramaDeBeneficiosController {
  constructor($rootScope, loginServices, programaDeBeneficiosService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.programaDeBeneficiosService = programaDeBeneficiosService;
    this.$log = $log;
    this.query = [];
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.init();
    }

    this.$rootScope.$on('loggedin', () => {
      this.init();
    });
  }

  init() {
    this.cities = [];
    this.getCities();
    this.getCategories();
  }

  getItems(){
    this.programaDeBeneficiosService.getData(this.query)
      .then((data) => {
        this.itens = data;
      });
  }

  getCities(){
    this.programaDeBeneficiosService.getCities()
      .then((data) => {
        if (data) {
          for (let i = 0; i < data.length; i++) {
            this.cities[data[i].id] = data[i];
          }

          this.getItems();
        }
      });
  }

  getCategories(){
    this.programaDeBeneficiosService.getCategories()
      .then((data) => {
        this.categories = data;
      });
  }
}
IndexProgramaDeBeneficiosController.$inject = ["$rootScope", "loginServices", "programaDeBeneficiosService", "$log"];

class IndexProgramaDeBeneficiosComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexProgramaDeBeneficiosController;
  }
}

const INDEX_PROGRAMADEBENEFICIOS_MODULE = angular
  .module("programaDeBeneficios.index", [])
  .component("indexProgramaDeBeneficiosComponent", new IndexProgramaDeBeneficiosComponent());

export { INDEX_PROGRAMADEBENEFICIOS_MODULE };
