const CheckinState = {
    name: "checkin",
    parent: 'root',
    url: "/checkin",
    component: "indexCheckinComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "checkin.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_CHECKIN_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { CheckinState };