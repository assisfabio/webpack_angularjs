"use strict";

import angular from "angular";

class IndexCheckinController {
  constructor($rootScope, loginServices, checkinService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.checkinService = checkinService;
    this.$log = $log;
    this.game = $rootScope.nextGame;
  }

  $onInit() {
    this.init();

    this.$rootScope.$on('loggedin', () => {
      this.init();
    });
    
    this.$rootScope.$on('next-game', (ev, args) => {
      this.game = args;
      this.init();
    });
  }

  init() {
    if (this.loginServices.isLogged()) {

      this.getProducts();
      
      if (this.game) {
        this.getUserInfo();
        this.getStadium(this.game.stadium);
      }
    }
  }

  getUserInfo(){
    this.checkinService.getUserInfo()
      .then((data) => {
        this.$log.log('userInfo:', data)
        this.partners = data;
        this.userPrincipal = (data && data.length > 0) && data.filter((obj) => {
          return obj.principal;
        })[0];
      });
  }

  getStadium(id){
    this.checkinService.getStadium(id)
      .then((data) => {
        this.stadium = data;
      });
  }

  getProducts(){
    this.checkinService.getProducts()
      .then((data) => {
        this.products = data;
      });
  }

  saveCheckin(userInfo, data){
    this.$log.log('CheckinPage -> saveCheckinFn:', userInfo, data);
  }

  saveAccess(userInfo, data){
    this.$log.log('CheckinPage -> saveAccessFn:', userInfo, data);
  }
}
IndexCheckinController.$inject = ["$rootScope", "loginServices", "checkinService", "$log"];

class IndexCheckinComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexCheckinController;
  }
}

const INDEX_CHECKIN_MODULE = angular
  .module("checkin.index", [])
  .component("indexCheckinComponent", new IndexCheckinComponent());

export { INDEX_CHECKIN_MODULE };
