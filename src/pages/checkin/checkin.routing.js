"use strict";
import { CheckinState } from "./checkin.states";

function CheckinRouting($stateProvider) {
  $stateProvider.state(CheckinState);
}
CheckinRouting.$inject = [ "$stateProvider"];

export { CheckinRouting };
