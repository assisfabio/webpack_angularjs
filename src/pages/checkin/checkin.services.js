let $ctrl;
class CheckinService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.apiUrl = APP_CONFIG.API.URL;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getUserInfo() {
    return $ctrl.$http.get(`mocks/user-info-checkin.json`)
    .then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }

  getStadium(id) {
    return $ctrl.$http.get(`mocks/stadium.json?id=${id}`)
    .then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }

  getProducts() {
    return $ctrl.$http.get('mocks/loja-de-vantagens.json')
      .then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }
}

CheckinService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { CheckinService };
