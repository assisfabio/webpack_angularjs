import angular from "angular";
import { CheckinRouting } from "./checkin.routing";
import { CheckinService } from "./checkin.services";

const CHECKIN_MODULE = angular
  .module("checkinModule", [])
  .config(CheckinRouting)
  .service('checkinService', CheckinService);

export { CHECKIN_MODULE };
