let $ctrl;
class JogosService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.jogos.getData.method,
      url: $ctrl.appConfig.endpoints.jogos.getData.url
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }
}

JogosService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { JogosService };
