const JogosState = {
    name: "jogos",
    url: "/jogos",
    component: "indexJogosComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "jogos.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_JOGOS_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { JogosState };