"use strict";
import { JogosState } from "./jogos.states";

function JogosRouting($stateProvider) {
  $stateProvider.state(JogosState);
}
JogosRouting.$inject = [ "$stateProvider"];

export { JogosRouting };
