import angular from "angular";
import { JogosRouting } from "./jogos.routing";
import { JogosService } from "./jogos.services";

const JOGOS_MODULE = angular
  .module("jogosModule", [])
  .config(JogosRouting)
  .service('jogosService', JogosService);

export { JOGOS_MODULE };
