"use strict";

import angular from "angular";
import "./index.scss";

class IndexJogosController {
  constructor($rootScope, loginServices, jogosService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.jogosService = jogosService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.jogosService.getData(this.type)
      .then((data) => {
        this.itens = data;
      });
  }
}
IndexJogosController.$inject = ["$rootScope", "loginServices", "jogosService", "$log"];

class IndexJogosComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexJogosController;
  }
}

const INDEX_JOGOS_MODULE = angular
  .module("jogos.index", [])
  .component("indexJogosComponent", new IndexJogosComponent());

export { INDEX_JOGOS_MODULE };
