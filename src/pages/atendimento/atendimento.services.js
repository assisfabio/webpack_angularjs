let $ctrl;
class AtendimentoService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.atendimento.getData.method,
      url: $ctrl.appConfig.endpoints.atendimento.getData.url
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }
}

AtendimentoService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { AtendimentoService };
