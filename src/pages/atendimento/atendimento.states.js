const AtendimentoState = {
    name: "atendimento",
    parent: 'root',
    url: "/atendimento",
    component: "indexAtendimentoComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "atendimento.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_ATENDIMENTO_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { AtendimentoState };