"use strict";
import { AtendimentoState } from "./atendimento.states";

function AtendimentoRouting($stateProvider) {
  $stateProvider.state(AtendimentoState);
}
AtendimentoRouting.$inject = [ "$stateProvider"];

export { AtendimentoRouting };
