import angular from "angular";
import { AtendimentoRouting } from "./atendimento.routing";
import { AtendimentoService } from "./atendimento.services";

const ATENDIMENTO_MODULE = angular
  .module("atendimentoModule", [])
  .config(AtendimentoRouting)
  .service('atendimentoService', AtendimentoService);

export { ATENDIMENTO_MODULE };
