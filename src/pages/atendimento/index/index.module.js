"use strict";

import angular from "angular";
import "./index.scss";

class IndexAtendimentoController {
  constructor($rootScope, loginServices, atendimentoService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.atendimentoService = atendimentoService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.atendimentoService.getData(this.type)
      .then((data) => {
        this.itens = data;
        this.show = data[0].id;
      });
  }
}
IndexAtendimentoController.$inject = ["$rootScope", "loginServices", "atendimentoService", "$log"];

class IndexAtendimentoComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexAtendimentoController;
  }
}

const INDEX_ATENDIMENTO_MODULE = angular
  .module("atendimento.index", [])
  .component("indexAtendimentoComponent", new IndexAtendimentoComponent());

export { INDEX_ATENDIMENTO_MODULE };
