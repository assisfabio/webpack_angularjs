"use strict";
import { ServicosState } from "./servicos.states";

function ServicosRouting($stateProvider) {
  $stateProvider.state(ServicosState);
}
ServicosRouting.$inject = [ "$stateProvider"];

export { ServicosRouting };
