let $ctrl;
class ServicosService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.servicos.getData.method,
      url: $ctrl.appConfig.endpoints.servicos.getData.url
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }
}

ServicosService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { ServicosService };
