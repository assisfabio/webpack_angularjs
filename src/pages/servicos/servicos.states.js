const ServicosState = {
    name: "servicos",
    parent: 'root',
    url: "/servicos",
    component: "indexServicosComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "servicos.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_SERVICOS_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { ServicosState };