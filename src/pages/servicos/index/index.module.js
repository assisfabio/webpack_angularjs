"use strict";

import angular from "angular";
import './index.scss';

class IndexServicosController {
  constructor($rootScope, loginServices, servicosService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.servicosService = servicosService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.servicosService.getData(this.type)
      .then((data) => {
        this.itens = data;
      });
  }
}
IndexServicosController.$inject = ["$rootScope", "loginServices", "servicosService", "$log"];

class IndexServicosComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexServicosController;
  }
}

const INDEX_SERVICOS_MODULE = angular
  .module("lojadevantagens.index", [])
  .component("indexServicosComponent", new IndexServicosComponent());

export { INDEX_SERVICOS_MODULE };
