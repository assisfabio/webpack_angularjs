import angular from "angular";
import { ServicosRouting } from "./servicos.routing";
import { ServicosService } from "./servicos.services";

const SERVICOS_MODULE = angular
  .module("servicosModule", [])
  .config(ServicosRouting)
  .service('servicosService', ServicosService);

export { SERVICOS_MODULE };
