"use strict";
import { AccountState } from "./account.states";

function AccountRouting($stateProvider) {
  $stateProvider.state(AccountState);
}
AccountRouting.$inject = [ "$stateProvider"];

export { AccountRouting };
