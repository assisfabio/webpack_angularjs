import angular from "angular";

import { AccountRouting } from "./account.routing";
import { AccountService } from "./account.services";

const ACCOUNT_MODULE = angular
  .module("accountModule", [])
  .config(AccountRouting)
  .service('accountService', AccountService);

export { ACCOUNT_MODULE };
