"use strict";

import angular from "angular";

let $ctrl;
class IndexAccountController {
  constructor($rootScope, loginServices, accountService, $log) {
    $ctrl = this;
    $ctrl.$rootScope = $rootScope;
    $ctrl.loginServices = loginServices;
    $ctrl.accountService = accountService;
    $ctrl.$log = $log;
  }

  $onInit() {
    if ($ctrl.loginServices.isLogged()) {
      $ctrl.getUser();
    }

    $ctrl.$rootScope.$on('loggedin', () => {
      $ctrl.getUser();
    });
  }

  getUser(){
    $ctrl.accountService.getUser()
      .then((data) => {
        $ctrl.user = data;
      });
  }

  savePersonalData(data) {
    $ctrl.$log.log('AccountIndex -> SavePersonalData ->', data);

    return $ctrl.accountService.setPersonalData(data)
      .then((result) => {
        $ctrl.user = angular.merge($ctrl.user, result);
        return result;
      });
  }

  cancelPersonalData() {
    $ctrl.$log.log('AccountIndex -> CancelPersonalData');
  }

  savePassword(data) {
    $ctrl.$log.log('AccountIndex -> SavePassword ->', data);
    return $ctrl.accountService.savePassword(data)
      .then((result) => {
        return result;
      });
  }

  cancelPassword() {
    $ctrl.$log.log('AccountIndex -> CancelPassword');
  }

  saveAddresses(data) {
    $ctrl.$log.log('AccountIndex -> SaveAddresses ->', data);
    return $ctrl.accountService.saveAddresses(data)
      .then((result) => {
        $ctrl.user.addressData = result.addressData;
        return result;
      });
  }

  addAddress(data) {
    $ctrl.$log.log('AccountIndex -> AddAddress ->', data);
    return $ctrl.accountService.addAddress(data)
      .then((result) => {
        let i = angular.copy($ctrl.user.addressData);
        i.unshift(result);
        $ctrl.user.addressData = i;
        return result;
      });
  }

  deleteAddress(data, index) {
    $ctrl.$log.log('AccountIndex -> DeleteAddress ->', data);
    return $ctrl.accountService.deleteAddress(data)
      .then((result) => {
        $ctrl.$log.log('AccountIndex -> DeleteAddress ->', result);
        if (result && result.result === 'SUCCESS') {
          let i = angular.copy($ctrl.user.addressData);
          i.splice(index, 1);
          $ctrl.$log.log(i, $ctrl.user.addressData)
          $ctrl.user.addressData = i;
        }
        return result;
      });
  }

  cancelAddresses() {
    $ctrl.$log.log('AccountIndex -> CancelAddresses');
  }

  saveContacts(data) {
    $ctrl.$log.log('AccountIndex -> SaveContacts ->', data);
    return $ctrl.accountService.saveContacts(data)
      .then((result) => {
        $ctrl.$log.log('index->', result);
        $ctrl.user.scheduleData = result.scheduleData;
        return result;
      });
  }

  addContact(data) {
    $ctrl.$log.log('AccountIndex -> AddContact ->', data);
    return $ctrl.accountService.addContact(data)
      .then((result) => {
        let i = angular.copy($ctrl.user.scheduleData);
        i.unshift(result);
        $ctrl.user.scheduleData = i;
        return result;
      });
  }

  deleteContact(data, index) {
    $ctrl.$log.log('AccountIndex -> DeleteContactData ->', data);
    return $ctrl.accountService.deleteContact(data)
      .then((result) => {
        if (result && result.result === 'SUCCESS') {
          let i = angular.copy($ctrl.user.scheduleData);
          i.splice(index, 1);
          $ctrl.user.scheduleData = i;
        }
        return result;
      });
  }

  cancelContacts() {
    $ctrl.$log.log('AccountIndex -> CancelContacts');
  }
}
IndexAccountController.$inject = ["$rootScope", "loginServices", "accountService", "$log"];

class IndexAccountComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexAccountController;
  }
}

const INDEX_ACCOUNT_MODULE = angular
  .module("account.index", [])
  .component("indexAccountComponent", new IndexAccountComponent());

export { INDEX_ACCOUNT_MODULE };
