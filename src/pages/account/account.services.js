let $ctrl;
class AccountService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getUser() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.getUser.method,
      url: $ctrl.appConfig.endpoints.account.getUser.url
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  setPersonalData(data){
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.setPersonalData.method,
      url: $ctrl.appConfig.endpoints.account.setPersonalData.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  savePassword(data) {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.savePassword.method,
      url: $ctrl.appConfig.endpoints.account.savePassword.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseData,
        $ctrl.messageService.errorHandler
      );
  }

  saveAddresses(data) {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.saveAddresses.method,
      url: $ctrl.appConfig.endpoints.account.saveAddresses.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  addAddress(data){
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.addAddress.method,
      url: $ctrl.appConfig.endpoints.account.addAddress.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  deleteAddress(id){
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.deleteAddress.method,
      url: $ctrl.appConfig.endpoints.account.deleteAddress.url(id)
    }).then(
        $ctrl.messageService.returnResponseData,
        $ctrl.messageService.errorHandler
      );
  }

  saveContacts(data) {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.saveContacts.method,
      url: $ctrl.appConfig.endpoints.account.saveContacts.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  addContact(data){
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.addContact.method,
      url: $ctrl.appConfig.endpoints.account.addContact.url,
      data: data
    }).then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  deleteContact(id){
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.account.deleteContact.method,
      url: $ctrl.appConfig.endpoints.account.deleteContact.url(id),
    }).then(
        $ctrl.messageService.returnResponseData,
        $ctrl.messageService.errorHandler
      );
  }
}

AccountService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { AccountService };
