const AccountState = {
    name: "account",
    parent: 'root',
    url: "/account",
    component: "indexAccountComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "account.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_ACCOUNT_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { AccountState };