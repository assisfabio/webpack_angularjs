"use strict";

import angular from "angular";
import './index.scss';

class IndexPageController {
  constructor(homeService, loginServices, $rootScope, $log) {
    this.homeService = homeService;
    this.loginServices = loginServices;
    this.$rootScope = $rootScope;
    this.$log = $log;

    this.serviceCarouselOptions = {
      autoWidth: false,
      dots: true,
      responsive: {
        0:{ items:1 },
        480:{ items:2 },
        768:{ items:3 },
        1024:{ items:4 }
      }
    }

    this.newsCarouselOptions = {
      autoWidth: false,
      dots: true,
      responsive: {
        0:{ items:1 },
        480:{ items:2 },
        768:{ items:3 }
      }
    }

    this.cardsCarouselOptions = {
      autoWidth: false,
      dots: true,
      responsive: {
        0:{ items:1 },
        600:{ items:2 }
      }
    }
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.init()
    }

    this.$rootScope.$on('loggedin', () => {
      this.init()
    });
  }

  init(){
    this.getProducts();
    this.getServices();
    this.getNews();
    this.getCards();
  }

  getProducts(){
    this.homeService.getProducts().then((data) => {
      this.products = data;
    });
  }

  getServices(){
    this.homeService.getServices().then((data) => {
      this.services = data;
    });
  }

  getNews(){
    this.homeService.getNews().then((data) => {
      this.news = data;
    });
  }

  getCards(){
    this.homeService.getCards().then((data) => {
      this.cards = data;
    });
  }

  goTo(state){
    this.$log.log(state);
  }
}
IndexPageController.$inject = ["homeService", "loginServices", "$rootScope", "$log"];

class IndexPageComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexPageController;
  }
}

const INDEX_PAGE_MODULE = angular
  .module("home.index", [])
  .component("indexPageComponent", new IndexPageComponent());

export { INDEX_PAGE_MODULE };
