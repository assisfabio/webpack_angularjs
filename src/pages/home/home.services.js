let $ctrl;
class HomeService {
    constructor(APP_CONFIG, $http, messageService, $log) {
        $ctrl = this;
        $ctrl.apiUrl = APP_CONFIG.API.URL;
        $ctrl.$http = $http;
        $ctrl.$log = $log;
        $ctrl.messageService = messageService;
    }

    getProducts() {
        return $ctrl.$http.get(`mocks/produtos.json`)
        .then(
            $ctrl.messageService.returnResponseMsgSaida,
            $ctrl.messageService.errorHandler
        );
    }

    getServices() {
        return $ctrl.$http.get(`mocks/servicos.json`)
        .then(
            $ctrl.messageService.returnResponseMsgSaida,
            $ctrl.messageService.errorHandler
        );
    }

    getNews() {
        return $ctrl.$http.get(`mocks/noticias.json`)
        .then(
            $ctrl.messageService.returnResponseMsgSaida,
            $ctrl.messageService.errorHandler
        );
    }

    getCards() {
        return $ctrl.$http.get(`mocks/carteiras.json`)
        .then(
            $ctrl.messageService.returnResponseMsgSaida,
            $ctrl.messageService.errorHandler
        );
    }
}
HomeService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { HomeService };