const HomeIndex = {
    name: "home",
    parent: 'root',
    url: "/home",
    component: "indexPageComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "home.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_PAGE_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { HomeIndex };