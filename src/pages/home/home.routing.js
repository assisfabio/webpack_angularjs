"use strict";
import { HomeIndex } from "./home.states";

function HomeRouting($stateProvider) {
  $stateProvider.state(HomeIndex);
}
HomeRouting.$inject = [ "$stateProvider"];

export { HomeRouting };
