import angular from "angular";
import { HomeRouting } from "./home.routing";
import { HomeService } from "./home.services";

const HOME_MODULE = angular
  .module("homeModule", [])
  .config(HomeRouting)
  .service('homeService', HomeService);

export { HOME_MODULE };
