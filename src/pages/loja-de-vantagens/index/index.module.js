"use strict";

import angular from "angular";
import './index.scss';

class IndexLojaDeVantagensController {
  constructor($rootScope, loginServices, lojaDeVantagensService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.lojaDeVantagensService = lojaDeVantagensService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.lojaDeVantagensService.getData(this.type)
      .then((data) => {
        this.itens = data;
      });
  }
}
IndexLojaDeVantagensController.$inject = ["$rootScope", "loginServices", "lojaDeVantagensService", "$log"];

class IndexLojaDeVantagensComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexLojaDeVantagensController;
  }
}

const INDEX_LOJADEVANTAGENS_MODULE = angular
  .module("lojadevantagens.index", [])
  .component("indexLojaDeVantagensComponent", new IndexLojaDeVantagensComponent());

export { INDEX_LOJADEVANTAGENS_MODULE };
