import angular from "angular";
import { LojaDeVantagensRouting } from "./loja-de-vantagens.routing";
import { LojaDeVantagensService } from "./loja-de-vantagens.services";

const LOJADEVANTAGENS_MODULE = angular
  .module("lojaDeVantagensModule", [])
  .config(LojaDeVantagensRouting)
  .service('lojaDeVantagensService', LojaDeVantagensService);

export { LOJADEVANTAGENS_MODULE };
