let $ctrl;
class LojaDeVantagensService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData() {
    return $ctrl.$http({
      method: $ctrl.appConfig.endpoints.loja.getData.method,
      url: $ctrl.appConfig.endpoints.loja.getData.url
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }
}

LojaDeVantagensService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { LojaDeVantagensService };
