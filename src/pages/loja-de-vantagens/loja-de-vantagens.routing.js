"use strict";
import { LojaDeVantagensState } from "./loja-de-vantagens.states";

function LojaDeVantagensRouting($stateProvider) {
  $stateProvider.state(LojaDeVantagensState);
}
LojaDeVantagensRouting.$inject = [ "$stateProvider"];

export { LojaDeVantagensRouting };
