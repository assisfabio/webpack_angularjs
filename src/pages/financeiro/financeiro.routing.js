"use strict";
import { FinanceiroState } from "./financeiro.states";

function FinanceiroRouting($stateProvider) {
  $stateProvider.state(FinanceiroState);
}
FinanceiroRouting.$inject = [ "$stateProvider"];

export { FinanceiroRouting };
