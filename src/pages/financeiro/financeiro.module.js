import angular from "angular";
import { FinanceiroRouting } from "./financeiro.routing";
import { FinanceiroService } from "./financeiro.services";

const FINANCEIRO_MODULE = angular
  .module("financeiroModule", [])
  .config(FinanceiroRouting)
  .service('financeiroService', FinanceiroService);

export { FINANCEIRO_MODULE };
