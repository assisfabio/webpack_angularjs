const FinanceiroState = {
    name: "financeiro",
    parent: 'root',
    url: "/financeiro",
    component: "indexFinanceiroComponent",
    lazyLoad: ($transition$) => {
        const $ocLazyLoad = $transition$.injector().get("$ocLazyLoad");

        return import(/* webpackChunkName: "financeiro.module" */ "./index/index.module")
            .then(mod => $ocLazyLoad.load(mod.INDEX_FINANCEIRO_MODULE))
            .catch(err => {
                throw new Error("Ooops, something went wrong, " + err);
            });
    }
};

export { FinanceiroState };