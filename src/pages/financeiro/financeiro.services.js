let $ctrl;
class FinanceiroService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    $ctrl = this;
    $ctrl.appConfig = APP_CONFIG;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
  }

  getData(data) {
    return $ctrl.$http({
      method: `${$ctrl.appConfig.endpoints.financial.paymentMethods.method}`,
      url: `${$ctrl.appConfig.endpoints.financial.paymentMethods.url}`,
      data: data
    }).then(
      $ctrl.messageService.returnResponseMsgSaida,
      $ctrl.messageService.errorHandler
    );
  }
}

FinanceiroService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { FinanceiroService };
