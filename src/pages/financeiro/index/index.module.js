"use strict";

import angular from "angular";
import "./index.scss";

class IndexFinanceiroController {
  constructor($rootScope, loginServices, financeiroService, $log) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
    this.financeiroService = financeiroService;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });
  }

  getItems(){
    this.financeiroService.getData(this.type)
      .then((data) => {
        this.paymentMethods = data;
      });
  }
}
IndexFinanceiroController.$inject = ["$rootScope", "loginServices", "financeiroService", "$log"];

class IndexFinanceiroComponent {
  constructor() {
    this.template = require("./index.view.html");
    this.controller = IndexFinanceiroController;
  }
}

const INDEX_FINANCEIRO_MODULE = angular
  .module("financeiro.index", [])
  .component("indexFinanceiroComponent", new IndexFinanceiroComponent());

export { INDEX_FINANCEIRO_MODULE };
