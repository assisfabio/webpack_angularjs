"use strict";

function PagesRouting($urlRouterProvider) {
  $urlRouterProvider.otherwise("/home");
}
PagesRouting.$inject = ["$urlRouterProvider"];

export { PagesRouting };
