// home.module.js

"use strict";

import angular from "angular";
import "angular-cookies";
import "angular-i18n/pt-br";
import "angular-translate";
import "angular-translate-loader-partial";
import "angular-translate-storage-cookie";
import { i18nLangs }  from "./languages";

/**
 * Config of Translate Module
 * @param $translateProvider
 */
function i18nConfig($translateProvider) {

    let $langKeys = [];
    for (let $lang in i18nLangs) {
        $langKeys.push($lang);
        $translateProvider.translations($lang, i18nLangs[$lang]);
    }

    $translateProvider.registerAvailableLanguageKeys($langKeys);

    $translateProvider.preferredLanguage('pt-BR');
    $translateProvider.fallbackLanguage('pt-BR');
    $translateProvider.useSanitizeValueStrategy('escape');

    // remember language
    $translateProvider.useCookieStorage();
}
i18nConfig.$inject = ["$translateProvider", "$translatePartialLoaderProvider"];

/**
 * Service of Translation module
 * @param $state
 * @param $http
 * @param $timeout
 * @param $translate
 * @returns {{get: {language: get.language, languages: get.languages}, set: {language: set.language}}}
 */
function i18nService ($state, $http, $translate){
    return {
        get: {
            language: function(){
                return $translate.use();
            },
            languages: function(){
                return $translate.getAvailableLanguageKeys();
            }
        },
        set: {
            language: function(lang){
                $translate.use(lang);
                $http.defaults.headers.common['Accept-Language'] = lang;
                $state.reload();
            }
        }
    };
}
i18nService.$inject = ["$state", "$http", "$translate"];

const I18N_MODULE = angular
.module("i18nModule", ['pascalprecht.translate', 'ngCookies'])
    .config(i18nConfig)
    .service("translateService", i18nService);

export { I18N_MODULE };
