import { pt_BR } from "./commons/pt-BR";
import { en } from "./commons/en";

const i18nLangs = {
    "en": en,
    "pt-BR": pt_BR
};

export { i18nLangs };