const pt_BR = {
  "account": {
    "index": {
      "title": "Minha Conta"
    },
    "changepicture": "Alterar"
  },
  "attendance": {
    "index": {
      "title": "Atendimento"
    }
  },
  "benefits": {
    "index": {
      "title": "Programa de Benefícios"
    }
  },
  "chairs": {
    "index": {
      "title": "Minhas Cadeiras"
    },
    "certificate": "Certificado",
    "chairs": "Cadeira",
    "issuedate": "Data de Emissão",
    "printedname": "Nome Impresso",
    "sector": "Setor",
    "status": "Situação",
    "validitydate": "Data de Validade",
  },
  "checkin": {
    "index": {
      "title": "Checkin",
      "accesstransfer": "Transferência de Acesso",
      "accesspurchase": "Compra de Acesso",
      "products": "Produtos"
    }
  },
  "financial": {
    "index": {
      "title": "Métodos de Pagamento"
    }
  },
  "games": {
    "index": {
      "title": "Próximos Jogos"
    }
  },
  "globals": {
    "active": "Ativo",
    "addtocart": "Adicionar ao Carrinho",
    "anotherpicture": "Outra foto",
    "dates": {
      "default": "EEEE, dd/MM/yyyy - HH:mm",
      "short": "dd/MM/yyyy"
    },
    "discountof": "{{discount}}% de desconto",
    "docheckin": "Fazer Check-In",
    "inactive": "Inativo",
    "informations": "Informações",
    "save": "Salvar",
    "takepicture": "Tirar Foto",
    "types": {
      "": {
        "label": "Valor",
        "placeholder": "Selecione o tipo"
      },
      "address": {
        "label": "Endereço",
        "placeholder": "rua, avenida, etc."
      },
      "cep": {
        "label": "CEP",
        "placeholder": "XXXXX-XXX"
      },
      "city": {
        "label": "Cidade",
        "placeholder": ""
      },
      "country": {
        "label": "País",
        "placeholder": ""
      },
      "complement": {
        "label": "Complemento",
        "placeholder": "Casa, apartamento, bloco"
      },
      "email": {
        "label": "E-mail",
        "placeholder": "nome@dominio.com"
      },
      "neighborhood": {
        "label": "Bairro",
        "placeholder": ""
      },
      "state": {
        "label": "Estado",
        "placeholder": "UF"
      },
      "number": {
        "label": "N°",
        "placeholder": "S/N° ou XXX"
      },
      "tel": {
        "label": "Telefone",
        "placeholder": "(XX) ?XXXX-XXXX"
      },
    },
    "validuntil": "Válido até {{date | date:('globals.dates.short'|translate)}}",
    "wantthis": "Quero Este"
  },
  "login": {
    "btns": {
      "join": "Não sou sócio. Quero me cadastrar!",
      "login": "Já tenho conta. Quero entrar!",
      "submit": "Enviar"
    },
    "forgot": {
      "link": "Solicitar uma nova senha",
      "submit": "Enviar",
      "success": "As instruçoes para a troca de senha foram enviadas para seu e-mail.",
      "usernotfound": "O CPF informado não foi encontrado. Verifique o número digitado ou entre em contato com o SAC."
    },
    "password": {
      "label":"Senha",
      "placeholder": ""
    },
    "title": "Faça parte do melhor programa sócio-torcedor do Brasil",
    "username": {
      "label":"Número do CPF",
      "placeholder": "xxx.xxx.xxx-xx"
    }
  },
  "join": {
    "title": "Associe-se já e faça parte do time!",
  },
  "messages": {
    "name": "Nome inválido"
  },
  "services": {
    "index": {
      "title": "Serviços"
    }
  },
  "store": {
    "index": {
      "title": "Loja de Vantagens"
    }
  },
  "user": {
    "loggouttext": "Sair",
    "myaccount": "Minha Conta", 
    "salutation": 'Olá',
    "photomodal": {
      "title": "Alterar Foto",
      "camerror": "A câmera não pode ser iniciada. Será que você deu acesso a ela?",
      "loadfromfile": "Carregue uma foto a partir de sua biblioteca ou câmera."
    }
  },
  "validation": {
    "email": "E-mail inválido",
    "cep": "CEP inválido",
    "cpf": "CPF inválido",
    "maxlength": "Este campo está inválido",
    "minlength": "Este campo está inválido",
    "number": "Este campo está inválido",
    "pattern": "Este campo está inválido",
    "parse": "Este campo está inválido",
    "required": "Este campo é obrigatório",
    "max": "A data não pode ser futura",
    "date": "Data inválida",
    "nome": "Digite nome e sobrenome",
    "tel": "Telefone inválido",
    "horaInvalida": "A hora não pode ser futura",
    "url": "Endereço inválido",
    "nomeLength": "Digite pelo menos três letras",
    "duplicated": "Valores já cadastrados",
    "minAs": "Valor alto demais",
    "maxAs": "Valor baixo demais",
    "compare": "Os valores não combinam"
  }
};

export { pt_BR };
