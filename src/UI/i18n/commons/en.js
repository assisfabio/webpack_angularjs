const en = {
  "account": {
    "index": {
      "title": "My Account"
    },
    "changepicture": "Change"
  },
  "attendance": {
    "index": {
      "title": "Attendance"
    }
  },
  "benefits": {
    "index": {
      "title": "Benefits Program"
    }
  },
  "chairs": {
    "index": {
      "title": "My Chairs"
    },
    "certificate": "Certificado",
    "chairs": "Chair",
    "issuedate": "Data de Emissão",
    "printedname": "Nome Impresso",
    "sector": "Setor",
    "status": "Situação",
    "validitydate": "Data de Validade",
  },
  "checkin": {
    "index": {
      "title": "Checkin",
      "accesstransfer": "Transferência de Acesso",
      "accesspurchase": "Compra de Acesso",
      "products": "Products"
    }
  },
  "financial": {
    "index": {
      "title": "Payment Methods"
    }
  },
  "games": {
    "index": {
      "title": "Next Games"
    }
  },
  "globals": {
    "active": "Active",
    "addtocart": "Adicionar ao Carrinho",
    "anotherpicture": "Outra foto",
    "dates": {
      "default": "EEEE, MM/dd/yyyy - HH:mm",
      "short": "MM/dd/yyyy"
    },
    "discountof": "{{discount}}% de desconto",
    "docheckin": "Fazer Check-In",
    "inactive": "Inactive",
    "informations": "Informações",
    "save": "Save",
    "takepicture": "Tirar Foto",
    "types": {
      "": {
        "label": "Valor",
        "placeholder": "Selecione o tipo"
      },
      "address": {
        "label": "Endereço",
        "placeholder": "street, avenue, ..."
      },
      "cep": {
        "label": "ZIP",
        "placeholder": "XXXXX-XXX"
      },
      "city": {
        "label": "City",
        "placeholder": ""
      },
      "country": {
        "label": "Country",
        "placeholder": ""
      },
      "complement": {
        "label": "Complemento",
        "placeholder": "Casa, apartamento, bloco"
      },
      "email": {
        "label": "Email",
        "placeholder": "nome@dominio.com"
      },
      "neighborhood": {
        "label": "Bairro",
        "placeholder": ""
      },
      "state": {
        "label": "Estado",
        "placeholder": "UF"
      },
      "number": {
        "label": "N°",
        "placeholder": "S/N° ou XXX"
      },
      "tel": {
        "label": "Phone",
        "placeholder": "(XX) ?XXXX-XXXX"
      },
    },
    "validuntil": "Válido até {{date | date:('globals.dates.short'|translate)}}",
    "wantthis": "Quero Este"
  },
  "login": {
    "btns": {
      "join": "Not a Member? Join!",
      "login": "Have a account? Login!",
      "submit": "Send"
    },
    "forgot": {
      "link": "Solicitar uma nova senha",
      "submit": "Send",
      "success": "As instruçoes para a troca de senha foram enviadas para seu e-mail.",
      "usernotfound": "O CPF informado não foi encontrado. Verifique o número digitado ou entre em contato com o SAC."
    },
    "password": {
      "label":"Password",
      "placeholder": ""
    },
    "title": "Faça parte do melhor programa sócio-torcedor do Brasil",
    "username": {
      "label":"CPF Number",
      "placeholder": "xxx.xxx.xxx-xx"
    }
  },
  "join": {
    "title": "Associe-se já e faça parte do time!",
  },
  "messages": {
    "name": "Invalid Name"
  },
  "services": {
    "index": {
      "title": "Services"
    }
  },
  "store": {
    "index": {
      "title": "Loja de Vantagens"
    }
  },
  "user": {
    "loggouttext": "loggout",
    "myaccount": "My Account", 
    "salutation": 'Hello',
    "photomodal": {
      "title": "Alterar Foto",
      "camerror": "A câmera não pode ser iniciada. Será que você deu acesso a ela?",
      "loadfromfile": "Carregue uma foto a partir de sua biblioteca ou câmera."
    }
  },
  "validation": {
    "email": "E-mail inválido",
    "cep": "CEP inválido",
    "cpf": "CPF inválido",
    "maxlength": "Este campo está inválido",
    "minlength": "Este campo está inválido",
    "number": "Este campo está inválido",
    "pattern": "Este campo está inválido",
    "parse": "Este campo está inválido",
    "required": "Este campo é obrigatório",
    "max": "A data não pode ser futura",
    "date": "Data inválida",
    "nome": "Digite nome e sobrenome",
    "tel": "Telefone inválido",
    "horaInvalida": "A hora não pode ser futura",
    "url": "Endereço inválido",
    "nomeLength": "Digite pelo menos três letras",
    "duplicated": "Valores já cadastrados",
    "minAs": "Valor alto demais",
    "maxAs": "Valor baixo demais",
    "compare": "Os valores não combinam"
  }
};

export { en };
