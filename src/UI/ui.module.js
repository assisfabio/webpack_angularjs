"use strict";
import angular from "angular";

import "./commons/commons.module";
import "./filters/filters.module";
import "./i18n/i18n.module";

const UI_MODULE = angular
  .module("ui", [
    "ui.commons",
    "ui.filters",
    "i18nModule"
  ]);

export { UI_MODULE };
