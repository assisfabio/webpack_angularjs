"use strict";
import angular from "angular";
import { isEmpty } from "./Object.filters";
import { nl2Tag } from "./String.filters";
import { isEmail, toBlob, isCpf, onlyNumbers, isTel, formatCpf, formatTel, formatDate, dateToSql, isCep, formatCep } from "./Utils.filters";


const COMMONS_MODULE = angular
    .module("ui.filters", [])
    // Numbers
    .filter("isCep", isCep)
    .filter("isCpf", isCpf)
    // Object
    .filter('isEmpty', isEmpty)
    // String
    .filter('nl2Tag', nl2Tag)
    // Utils
    .filter('dateToSql', dateToSql)
    .filter('formatCep', formatCep)
    .filter('formatCpf', formatCpf)
    .filter('formatDate', formatDate)
    .filter('formatTel', formatTel)
    .filter('isEmail', isEmail)
    .filter('isTel', isTel)
    .filter('onlyNumbers', onlyNumbers)
    .filter('toBlob', toBlob);

export { COMMONS_MODULE };
