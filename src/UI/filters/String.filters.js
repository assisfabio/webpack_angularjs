function nl2Tag() {
    return function (text, tag) {
        return text.replace(new RegExp('\\n', 'g'), tag || '<br />');
    }
}

export { nl2Tag };