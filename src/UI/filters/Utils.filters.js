import angular from "angular";

function isCep() {
    return function (value) {
        return (value && value !== '') ? value.toString().length === 8 : false;
    };
}

function formatCep() {
    return function (value) {
        if (value && value !== '') {
            value = value.toString().replace(/\D/g,"");
            const a = value.split('');
    
            value = '';
            for (let i = 0; i < a.length; i++) {
                value += a[i];
                if (i === 4 && a.length > 5) {
                    value += '-';
                }
    
                if (i === 7) {
                    break;
                }
            }
        }
        return value;
    };
}

function isCpf () {
    return function (cpf) {
        if (cpf) {
            cpf = cpf.replace(/[^\d]+/g, '');
            
            let numeros,
            digitos,
            soma,
            resultado,
            digitos_iguais;

            digitos_iguais = 1;

            if (cpf.length < 11) {
                return false;
            }

            for (let i = 0; i < cpf.length - 1; i++) {
                if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
                    digitos_iguais = 0;
                    break;
                }
            }

            if (!digitos_iguais) {
                numeros = cpf.substring(0, 9);
                digitos = cpf.substring(9);
                soma = 0;
                for (let i = 10; i > 1; i--) {
                    soma += numeros.charAt(10 - i) * i;
                }

                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

                if (resultado !== parseFloat(digitos.charAt(0))) {
                    return false;
                }
                numeros = cpf.substring(0, 10);
                soma = 0;

                for (let i = 11; i > 1; i--) {
                    soma += numeros.charAt(11 - i) * i;
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

                if (resultado !== parseFloat(digitos.charAt(1))) {
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
}

function formatCpf() {
    return function (value) {
        if (value && value !== '') {
            value = value.replace(/\D/g,"");
            const a = value.split('');
    
            value = '';
            for (let i = 0; i < a.length; i++) {
                value += a[i];
                if ((i === 2 && a.length > 3) || (i === 5 && a.length > 6)) {
                    value += '.';
                }
                if (i === 8 && a.length > 9) {
                    value += '-';
                }
    
                if (i === 10) {
                    break;
                }
            }
        }
        return value;
    };
}

function formatDate () {
    return (value) => {
        if (value && value !== '') {
            value = value.replace(/\D/g,"");
            const a = value.split('');
    
            value = '';
            for (let i = 0; i < a.length; i++) {
                value += a[i];
                if ((i === 1 && a.length > 2) || (i === 3 && a.length > 4)) {
                    value += '/';
                }
    
                if (i === 7) {
                    break;
                }
            }
        }
        return value;
    }
}

function dateToSql ($filter) {
    return (value) => {
        if (value && value !== '') {
            const format = $filter('translate')('globals.dates.short').split('/');
            const data = value.split('/');
    
            const arr = [];
            arr[format[0]] = data[0];
            arr[format[1]] = data[1];
            arr[format[2]] = data[2];
    
            return arr['yyyy'] + '-' + arr['MM'] + '-' + arr['dd'];
        }
        return value;
    }
}
dateToSql.$inject = ["$filter"];

function isTel () {
    return function (tel) {
        if (angular.isDefined(tel) && tel !== '') {
            try {
                tel = (tel) ? tel.toString().replace(/[^\d]+/g, '') : tel.toString();
                let ddd = (tel.length >= 2) ? tel.substring(0,2) : tel;
                let num = (tel.length > 2) ? tel.substring(2, tel.length) : '';

                if (ddd.length !== 2 || ddd <= 10 || ddd >= 100) {
                    return false;
                }
      
                if ((num.length !== 8 && num.length !== 9) || (num.length === 9 && parseInt(num.substring(0,1)) !== 9)) {
                    return false;
                }
                
                return true;

            } catch (e) {
                return false;
            }
        }
    }
}

function formatTel () {
    return function (tel) {
        if (angular.isDefined(tel) && tel !== '') {
            try {
                tel = (tel) ? tel.toString().replace(/[^\d]+/g, '') : tel.toString();
                let ret = '';
                let ddd = (tel.length >= 2) ? tel.substring(0, 2) : tel;
                let num = (tel.length > 2) ? tel.substring(2, tel.length) : '';

                if (ddd.length >= 1) {
                    ret += '(' + ddd;
                }

                if (num.length >= 1) {
                    ret += ') ';

                    if (num.length <= 4) {
                        ret += num.substring(0, num.length);
                    }
                    else if (num.length <= 8) {
                        ret += num.substring(0, 4) + '-' + num.substring(4, num.length);
                    }
                    else {
                        if (parseInt(num.substring(0, 1)) === 9) {
                            ret += num.substring(0, 5) + '-' + num.substring(5, 9);
                        }
                        else {
                            ret += num.substring(0, 4) +'-'+ num.substring(4, 8);
                        }
                    }
                }
                return ret;
            } catch (e) {
                return tel;
            }
        }
    }
}

function isEmail () {
    return function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; // eslint-disable-line no-useless-escape
        return re.test(email);
    };
}

function onlyNumbers() {
    return function (value) {
        if (value) {
            return value.replace(/\D/g,"");
        }
        return value;
    }
}

function toBlob () {
    return function (dataURI) {
        let byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
            byteString = atob(dataURI.split(',')[1]);
        }
        else {
            byteString = decodeURI(dataURI.split(',')[1]);
        }

        // separate out the mime component
        const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        const ia = new Uint8Array(byteString.length);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type: mimeString});
    };
}

export { dateToSql, formatCep, formatCpf, formatDate, formatTel, isCep, isCpf, isEmail, isTel, onlyNumbers, toBlob };