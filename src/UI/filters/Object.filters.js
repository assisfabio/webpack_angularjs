/**
 * Returns if object has no property
 * @param obj
 * @return boolean
 */
function isEmpty () {
    return (obj) => {
        for (let bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
}

export { isEmpty };