"use strict";
import angular from "angular";

import 'owl.carousel';

import { ProductSlideComponent } from "./product-slide/product-slide.component";
import { OwlCarousel, OwlCarouselItem } from "./owl.directives";

const SLIDES_MODULE = angular
    .module('component.slides', [])
    .directive('owlCarousel', OwlCarousel)
    .directive('owlCarouselItem', OwlCarouselItem)
    .component('f2pProductSlide', new ProductSlideComponent());

export { SLIDES_MODULE };
