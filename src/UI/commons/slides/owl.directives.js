/*global $*/
import './owl.defaults.scss';

function OwlCarousel ($rootScope, $timeout, $filter) {
    return {
        restrict: 'A',
        transclude: false,
        link: function (scope, element, attrs) {
            var owl = false;
            scope.initCarousel = function (element) {
            // provide any default options you want
            owl = $(element);
            scope.item = {};
            scope.defaultOptions = {
                "autoWidth": true,
                "dots": false,
                "nav": true,
                "navRewind": false,
                'navText': ['<span class="fas fa-chevron-left"></span>', '<span class="fas fa-chevron-right"></span>']
            };
            var customOptions = scope.$eval(attrs.options);
            // combine the two options objects
            for (let key in customOptions) {
                scope.defaultOptions[key] = customOptions[key];
            }

            $timeout(() => {
                if (scope.defaultOptions["autoWidth"] === true) {
                    $(element).children().each(function (){
                        $(this).width($(this).width());
                    });
                }
                // init carousel
                owl.owlCarousel(scope.defaultOptions);
                owl.on('changed.owl.carousel', (event) => {
                    scope.item = event.item;
                });

                }, 500);
            };

            $rootScope.$on('refresh-carousel', (ev, params) => {
                if (owl && params) {
                    for (let i = 0; i < params.targetsId.length; i++) {
                        if (owl.is('#' + params.targetsId[i])) {
                            owl.trigger('destroy.owl.carousel');
                            $timeout(() => {
                                owl.owlCarousel(scope.defaultOptions);
                            }, 500);
                        }
                    }
                }
            });

            $rootScope.$on('carousel-goto', () => {
                var teste = scope.item;
                $timeout(() => {
                    if (!$filter('isEmpty')(teste)){
                        owl.trigger('to.owl.carousel', teste.index);
                    }
                }, 600);
            })
        }
    };
}
OwlCarousel.$inject = ["$rootScope", "$timeout", "$filter"];

function OwlCarouselItem ($timeout) {
    return {
      restrict: 'A',
      transclude: false,
      link: (scope, element) => {
        // wait for the last item in the ng-repeat then call init
        if (scope.$last) {
          $timeout(scope.initCarousel(element.parent()), 500);
        }
      }
    };
  }
OwlCarouselItem.$inject = ["$timeout"];

export { OwlCarousel, OwlCarouselItem };