import './product-slide.scss';

class ProductSlideController {
  constructor($rootScope, $log, cartServices) {
    this.$rootScope = $rootScope;
    this.$log = $log;
    this.cartServices = cartServices;

    this.options = {
      autoWidth: false,
      dots: true,
      responsive: {
        0: {
          nav: false,
          items: 1
        },
        480: {
          nav: true,
          items: 1
        } 
      }
    }
  }

  $onInit() {
  }

  addToCart(item) {
    this.cartServices.setItem(item);
  }
}
ProductSlideController.$inject = ["$rootScope", "$log", "cartServices"];

class ProductSlideComponent {
  constructor() {
    this.controller = ProductSlideController;
    this.restrict = 'E',
    this.template = require("./product-slide.view.html");
    this.transclude = true;
    this.bindings = {
        products: "<",
        cssClass: "<",
        id: "<",
        btnText: '<'
    }
  }
}

export { ProductSlideComponent };
