"use strict";
import angular from "angular";

// import { MenuComponent } from "./menu.component";
import { MessageService } from "./message.services";

const MESSAGE_MODULE = angular
    .module("component.message", [])
    .service('messageService', MessageService)

export { MESSAGE_MODULE };
