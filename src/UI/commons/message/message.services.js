let _this;
class MessageService {
  constructor($log) {
    _this = this;
    _this.$log = $log;
  }

  errorHandler(response) {
    /**
     * TODO
     * Abrir Modal com a msg de Erro
     **/
    _this.$log.log(response);

    return null;
  }

  successHandler(response) {
    function verifySuccessResponse(resp) {
      return (resp.data.result !== 'SUCCESS') ? _this.errorHandler(resp) : resp;
    }
    return verifySuccessResponse(response);
  }

  returnResponseData(response) {
    const resp = _this.successHandler(response);
    return (resp) ? resp.data : null;
  }

  returnResponseMsgSaida(response) {
    const resp = _this.successHandler(response);
    return (resp) ? resp.data.msgSaida : null;
  }

  returnResponseFirstItem(response) {
    const resp = _this.successHandler(response);
    return (resp) ? resp.data.msgSaida[0] : null;
  }
}

MessageService.$inject = ["$log"];

export { MessageService };
