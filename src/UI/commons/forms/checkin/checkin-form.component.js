"use strict";

class CheckinFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  save() {
    const form = this.$scope.formCheckin;
    if (form.$invalid || !form.$dirty) {
      return false;
    }

    this.$log.log(form);
    if (typeof this.onSave === 'function') {
      this.onSave(this.userInfo, {
        sector: form.sectorCheckin.$modelValue,
        access: form.accessCheckin.$modelValue
      });
    }
  }
}
CheckinFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class CheckinFormComponent {
  constructor() {
    this.template = require("./checkin-form.view.html");
    this.controller = CheckinFormController;
    this.bindings = {
        sectors: "<",
        userInfo: "<",
        onSave: "<"
    }
  }
}

export { CheckinFormComponent };