let $ctrl;
class FormService {
    constructor(APP_CONFIG, $http, messageService, $log) {
        $ctrl = this;
        $ctrl.apiUrl = APP_CONFIG.API.URL;
        $ctrl.$http = $http;
        $ctrl.messageService = messageService;
        $ctrl.$log = $log;
    }

    getInterests() {
        return $ctrl.$http.get(`mocks/interesses.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getFamilyIncome() {
        return $ctrl.$http.get(`mocks/rendafamiliar.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getProfession() {
        return $ctrl.$http.get(`mocks/profissao.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getNationality() {
        return $ctrl.$http.get(`mocks/nacionalidade.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getGender() {
        return $ctrl.$http.get(`mocks/genero.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getGameFrequency() {
        return $ctrl.$http.get(`mocks/frequenciaestadio.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getMarritialStatus(){
        return $ctrl.$http.get(`mocks/estadocivil.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getEducation(){
        return $ctrl.$http.get(`mocks/escolaridade.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getContactTypes(){
        return $ctrl.$http.get(`mocks/tipo-contatos.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getAddressTypes(){
        return $ctrl.$http.get(`mocks/tipo-enderecos.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }

    getAddressByCep(){
        return $ctrl.$http.get(`mocks/cep.json`)
            .then(
                $ctrl.messageService.returnResponseMsgSaida,
                $ctrl.messageService.errorHandler
            );
    }
}
FormService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { FormService };