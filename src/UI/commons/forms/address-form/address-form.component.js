"use strict";

// import angular from "angular";

class AddressFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  $onInit(){
    this.getAddressTypes();
  }

  $onChanges(changes){
    if (changes.addresses && changes.addresses.currentValue) {
      this.init();
    }
  }

  getAddressTypes(){
    this.formsService.getAddressTypes().then((data) => {
      this.addressTypes = data;
      this.init()
    })
  }

  getAddressByCep(cep, obj){
    this.formsService.getAddressByCep(cep).then((data) => {
      if (data) {
        obj.address = data.address;
        obj.state = data.state;
        obj.city = data.city;
        obj.neighborhood = data.neighborhood;

        obj.cepFound = true;
      }
      else {
        obj.cepFound = false;
      }
    })
  }

  init() {
    if (this.addresses && this.addressTypes) {
      this.addressData = [];
      for (let i = 0; i < this.addresses.length; i++) {
        this.addressData.push({
          "id": this.addresses[i].id,
          "type": this.addressTypes.filter((obj) => obj.id == this.addresses[i].type)[0],
          "postalCode": this.addresses[i].postalCode,
          "address": this.addresses[i].address,
          "number": this.addresses[i].number,
          "complement": this.addresses[i].complement,
          "neighborhood": this.addresses[i].neighborhood,
          "city": this.addresses[i].city,
          "state": this.addresses[i].state,
          "country": this.addresses[i].country
        })
      }
    }
  }

  onSave() {
    const form = this.$scope.formAddress;
    
    if (form.$invalid || !form.$dirty) {
      return false;
    }

    if (typeof this.save === 'function') {
      let dataToSend = [];
      for (let i = 0; i < this.addressData.length; i++) {
        let item = {
          "id": this.addressData[i].id,
          "type": this.addressData[i].id,
          "postalCode": this.addressData[i].postalCode,
          "address": this.addressData[i].address,
          "number": this.addressData[i].number,
          "complement": this.addressData[i].complement,
          "neighborhood": this.addressData[i].neighborhood,
          "city": this.addressData[i].city,
          "state": this.addressData[i].state,
          "country": this.addressData[i].country
        }
        dataToSend.push(item)
      }
      this.save(dataToSend);
    }
  }

  onCancel() {
    if (typeof this.cancel === 'function') {
      this.cancel();
    }
    this.addressData = [];
    this.init();
  }

  addAddress(){
    const form = this.$scope.formNewAddress;

    if (form.$invalid || !form.$dirty) {
      return false;
    }

    this.add(this.newItem).then(() => {
      this.$scope.formAddress.$setDirty();
      this.cancelNewAddress();
    });
  }

  deleteAddress(address, index){
    this.delete(address.id, index).then((data) => {
      if (data && data.result === 'SUCCESS') {
        this.$scope.formAddress.$setDirty();
      }
    });
  }

  cancelNewAddress() {
    const form = this.$scope.formNewAddress;

    this.newItem = {};

    form.$setPristine();
    form.$setUntouched();
    
    this.showNew = false;
  }
}
AddressFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class AddressFormComponent {
  constructor() {
    this.template = require("./address-form.view.html");
    this.controller = AddressFormController;
    this.bindings = {
        addresses: "<",
        add: "<",
        save: "<",
        cancel: "<",
        delete: "<",
        showNew: "="
    }
  }
}

export { AddressFormComponent };