class InputTextController {
    constructor($log){
        this.$log = $log;
    }
    
    $onInit() {
        switch(this.inputType) {
            case 'cep':
            case 'cpf':
            case 'tel':
            case 'number':
            case 'date':
            case 'currency':
                this.type = 'tel';
                break;

            case 'email':
                this.type = 'email';
                break;

            case 'password':
                this.type = 'password';
                break;
            default:
                this.type = 'text';
                break;
        }
    }
}
InputTextController.$inject = ["$log"];

/**
 * @example
 * <f2p-input-text
        data-model="$ctrl.name"
        data-container-class="conteiner-class"
        data-input-class="input-class"
        data-name="name"
        data-label="Nome"
        data-placeholder="Nome completo"
        data-required="true"
    ></f2p-input-text>
 */
class InputTextComponent {
    constructor() {
        this.controller = InputTextController;
        this.restrict = 'E',
        this.require = {
            form: '^form'
        },
        this.template = require("./input-text.view.html");
        this.transclude = true;
        this.bindings = {
            containerClass: "@",
            id: "@",
            inputClass: "@",
            inputType: "@",
            label: "@",
            labelClass: "@",
            maxLength: "@",
            minLength: "@",
            model: "=",
            name: "@",
            placeholder: "@",
            required: "@",
            compareWith: "<",
            autocomplete: "@",
            readonly: "<"
        };
    }
}
export { InputTextComponent };