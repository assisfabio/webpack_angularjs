"use strict";

class PasswordFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  onSave() {
    const form = this.$scope.formPassword;
    if (form.$invalid || !form.$dirty) {
      return false;
    }

    this.save({
      oldPass: form.oldPass.$modelValue,
      newPass: form.pass.$modelValue
    })
    .then((data) => {
      if (data && data.result === 'SUCCESS') {

        form.oldPass.$setViewValue('');
        form.oldPass.$processModelValue();
        form.pass.$setViewValue('');
        form.pass.$processModelValue();
        form.confirmPass.$setViewValue('');
        form.confirmPass.$processModelValue();

        form.$setPristine();
        form.$setUntouched();
      }
    });
  }

  onCancel() {
    const form = this.$scope.formPassword;
    if (typeof this.cancel === 'function') {
      this.cancel();
    }

    form.$setPristine();
    form.$setUntouched();
  }
}
PasswordFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class PasswordFormComponent {
  constructor() {
    this.template = require("./password-form.view.html");
    this.controller = PasswordFormController;
    this.bindings = {
        userId: "<",
        save: "<",
        cancel: "<"
    }
  }
}

export { PasswordFormComponent };