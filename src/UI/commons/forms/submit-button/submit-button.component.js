/*global $*/
import angular from "angular";

class SubmitButtonController {
    constructor($log){
        this.$log = $log;
    }
    
    $onInit() {
        this.cssClass = (this.cssClass) ? this.cssClass : 'btn btn-primary';
    }

    btnClick() {
        if (this.form.$invalid) {
            let count = 0;
            angular.forEach(this.form.$error, (field) => {
                angular.forEach(field, (errorField) => {
                    errorField.$setTouched(true);
                    if (count === 0) {
                        $(errorField.$$element).focus();
                        // $(errorField.$$element).blur();
                    }
                    count++;
                });
            });
        }
        
        if (typeof this.onClick === 'function') {
            this.onClick();
        }
    }
}
SubmitButtonController.$inject = ["$log"];

class SubmitButtonComponent {
    constructor() {
        this.controller = SubmitButtonController;
        this.restrict = 'E',
        this.require = {
            form: '^form'
        },
        this.template = require("./submit-button.view.html");
        this.transclude = true;
        this.bindings = {
            id: "@",
            cssClass: "@",
            label: "@",
            onClick: "&?"
        };
    }
}
export { SubmitButtonComponent };