/**
 * @example
 * <f2p-input-text
        data-model="$ctrl.name"
        data-container-class="conteiner-class"
        data-input-class="input-class"
        data-name="name"
        data-label="Nome"
        data-placeholder="Nome completo"
        data-required="true"
    ></f2p-input-text>
 */
class InputCheckComponent {
    constructor() {
        this.restrict = 'E',
        this.require = {
            form: '^form'
        },
        this.template = require("./input-check.view.html");
        this.transclude = true;
        this.bindings = {
            containerClass: "@",
            description: "@",
            id: "@",
            inputClass: "@",
            label: "@",
            labelClass: "@",
            model: "=",
            name: "@",
            required: "@",
        };
    }
}
export { InputCheckComponent };