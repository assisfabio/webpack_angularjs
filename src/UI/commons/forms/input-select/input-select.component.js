/**
 * @example
 * <f2p-input-select
        data-model="$ctrl.name"
        data-container-class="conteiner-class"
        data-input-class="input-class"
        data-name="name"
        data-label="Nome"
        data-required="true"
    ></f2p-input-select>
 */
class InputSelectComponent {
    constructor() {
        this.restrict = 'E',
        this.require = {
            form: '^form'
        },
        this.template = require("./input-select.view.html");
        this.transclude = true;
        this.bindings = {
            containerClass: "@",
            id: "@",
            inputClass: "@",
            label: "@",
            labelClass: "@",
            model: "=",
            name: "@",
            options: "<",
            required: "@",
        };
    }
}
export { InputSelectComponent };