"use strict";
import "./contact-form.scss";

class ContactFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  $onInit(){
    this.getContactTypes();
  }

  $onChanges(changes){
    if (changes.contacts && changes.contacts.currentValue) {
      this.init();
    }
  }

  init() {
    if (this.contacts && this.contactTypes) {
      this.contactData = [];
      for (let i = 0; i < this.contacts.length; i++) {
        this.contactData.push({
          "id": this.contacts[i].id,
          "type": this.contactTypes.filter((obj) => obj.id == this.contacts[i].type)[0],
          "value": this.contacts[i].value
        })
      }
    }
  }

  getContactTypes(){
    this.formsService.getContactTypes().then((data) => {
      this.contactTypes = data;
      this.init()
    })
  }

  onSave() {
    const form = this.$scope.formContact;

    if (form.$invalid || !form.$dirty) {
      return false;
    }

    if (typeof this.save === 'function') {
      let dataToSend = [];
      for (let i = 0; i < this.contactData.length; i++) {
        let item = {
          "id": this.contactData[i].id,
          "type": this.contactData[i].type.id,
          "value": this.contactData[i].value
        }
        dataToSend.push(item)
      }
      this.save(dataToSend);
    }
  }

  onCancel() {
    if (typeof this.cancel === 'function') {
      this.cancel();
    }
    this.contactData = [];
    this.init();
  }

  addContact(){
    const form = this.$scope.formNewContact;

    if (form.$invalid || !form.$dirty) {
      return false;
    }

    this.add(this.newItem).then(() => {
      this.$scope.formContact.$setDirty();
      this.cancelNewContact();
    });
  }

  deleteContact(contact, index){
    this.delete(contact.id, index).then((data) => {
      if (data && data.result === 'SUCCESS') {
        this.$scope.formContact.$setDirty();
      }
    });
  }

  cancelNewContact() {
    const form = this.$scope.formNewContact;

    form.$setPristine();
    form.$setUntouched();

    form.newContactValue.$setViewValue('');
    form.newContactValue.$processModelValue();
    form.newContactValue.$setPristine();
    form.newContactValue.$setUntouched();

    this.showNew = false;
  }
}
ContactFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class ContactFormComponent {
  constructor() {
    this.template = require("./contact-form.view.html");
    this.controller = ContactFormController;
    this.bindings = {
        contacts: "<",
        add: "<",
        save: "<",
        cancel: "<",
        delete: "<",
        showNew: "="
    }
  }
}

export { ContactFormComponent };