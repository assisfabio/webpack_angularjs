"use strict";

class AccessPurchaseFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  $onInit(){
    
  }

  save() {
    const form = this.$scope.formAccessPurchase;

    if (form.$invalid || !form.$dirty) return false;

    
    const dataToSend = [];

    // Get selected Checks
    for (const key in form) {
      if (form.hasOwnProperty(key) && key.indexOf('partner[') === 0 && key.indexOf('].check') > 8 && form[key].$modelValue) {
        dataToSend.push(this.purchase[key.replace('partner[', '').replace('].check', '')])
      }
    }

    if (dataToSend.length <= 0) return false;

    this.$log.log(this.purchase);
    this.$log.log(dataToSend);
    
    if (typeof this.onSave === 'function') {
      this.onSave();
    }
  }
}
AccessPurchaseFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class AccessPurchaseFormComponent {
  constructor() {
    this.template = require("./access-purchase-form.view.html");
    this.controller = AccessPurchaseFormController;
    this.bindings = {
        sectors: "<",
        partners: "<",
        onSave: "<"
    }
  }
}

export { AccessPurchaseFormComponent };