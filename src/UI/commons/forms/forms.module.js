"use strict";
import angular from "angular";
import "./forms.scss";
import { FormService } from "./forms.service";
import { InputBehavior } from "./inputs.directive";
import { InputTextComponent } from "./input-text/input-text.component";
import { PersonalFormComponent } from "./personal/personal-form.component";
import { InputSelectComponent } from "./input-select/input-select.component";
import { InputCheckComponent } from "./input-check/input-check.component";
import { SubmitButtonComponent } from "./submit-button/submit-button.component";
import { PasswordFormComponent } from "./password/password-form.component";
import { CheckinFormComponent } from "./checkin/checkin-form.component";
import { AccessFormComponent } from "./access/access-form.component";
import { AccessPurchaseFormComponent } from "./access-purchase-form/access-purchase-form.component";
import { ContactFormComponent } from "./contact-form/contact-form.component";
import { AddressFormComponent } from "./address-form/address-form.component";

const FORMS_MODULE = angular
    .module("formsModule", [])
    .service("formsService", FormService)
    .directive("inputBehavior", InputBehavior)
    .component('f2pInputText', new InputTextComponent())
    .component('f2pInputSelect', new InputSelectComponent())
    .component('f2pInputCheck', new InputCheckComponent())
    .component("f2pSubmitButton", new SubmitButtonComponent())
    .component('personalForm', new PersonalFormComponent())
    .component('passwordForm', new PasswordFormComponent())
    .component('checkinForm', new CheckinFormComponent())
    .component('accessForm', new AccessFormComponent())
    .component('accessPurchaseForm', new AccessPurchaseFormComponent())
    .component('contactForm', new ContactFormComponent())
    .component('addressForm', new AddressFormComponent());

export { FORMS_MODULE };
