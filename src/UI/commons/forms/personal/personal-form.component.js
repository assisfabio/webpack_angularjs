"use strict";
import angular from "angular";

class PersonalFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  $onInit(){
    if (!this.interestList || !this.interestList.length) {
      this.formsService.getInterests()
        .then((data) => {
          this.interestList = data;
          this.init();
        });
    }

    if (!this.familyIncomeList || !this.familyIncomeList.length) {
      this.formsService.getFamilyIncome()
        .then((data) => {
          this.familyIncomeList = data;
          this.init();
        });
    }

    if (!this.professionList || !this.professionList.length) {
      this.formsService.getProfession()
        .then((data) => {
          this.professionList = data;
          this.init();
        });
    }

    if (!this.nationalityList || !this.nationalityList.length) {
      this.formsService.getNationality()
        .then((data) => {
          this.nationalityList = data;
          this.init();
        });
    }

    if (!this.genreList || !this.genreList.length) {
      this.formsService.getGender()
        .then((data) => {
          this.genreList = data;
          this.init();
        });
    }

    if (!this.gameFrequencyList || !this.gameFrequencyList.length) {
      this.formsService.getGameFrequency()
        .then((data) => {
          this.gameFrequencyList = data;
          this.init();
        });
    }

    if (!this.marritialStatusList || !this.marritialStatusList.length) {
      this.formsService.getMarritialStatus()
        .then((data) => {
          this.marritialStatusList = data;
          this.init();
        });
    }

    if (!this.educationList || !this.educationList.length) {
      this.formsService.getEducation()
        .then((data) => {
          this.educationList = data;
          this.init();
        });
    }
  }
  
  $onChanges(changes){
    if (changes.personalData && changes.personalData.currentValue) {
      this.userData = angular.copy(this.personalData);
      this.init();
    }
  }

  init() {
    if (this.userData) {

      if (!this.userInterest && this.interestList){
        this.userInterest = this.interestList.filter((obj) => {
          return obj.id == this.personalData.interest;
        })[0] || this.interestList[0];
      }

      if (!this.userFamilyIncome && this.familyIncomeList){
        this.userFamilyIncome = this.familyIncomeList.filter((obj) => {
          return obj.id == this.personalData.familyIncome;
        })[0] || this.familyIncomeList[0];
      }

      if (!this.userProfession && this.professionList){
        this.userProfession = this.professionList.filter((obj) => {
          return obj.id == this.personalData.profession;
        })[0] || this.professionList[0];
      }

      if (!this.userNationality && this.nationalityList){
        this.userNationality = this.nationalityList.filter((obj) => {
          return obj.id == this.personalData.nationality;
        })[0] || this.nationalityList[0];
      }

      if (!this.userGenre && this.genreList) {
        this.userGenre = this.genreList.filter(obj => {
          return obj.id == this.personalData.sex;
        })[0] || this.genreList[0];
      }

      if (!this.userGameFrequency && this.gameFrequencyList){
        this.userGameFrequency = this.gameFrequencyList.filter((obj) => {
          return obj.id == this.personalData.frequencyGame;
        })[0] || this.gameFrequencyList[0];
      }

      if (!this.userMarritialStatus && this.marritialStatusList){
        this.userMarritialStatus = this.marritialStatusList.filter((obj) => {
          return obj.id == this.personalData.marritialStatus;
        })[0] || this.marritialStatusList[0];
      }

      if (!this.userEducation && this.educationList){
        this.userEducation = this.educationList.filter((obj) => {
          return obj.id == this.personalData.education;
        })[0] || this.educationList[0];
      }

    }
  }

  onSave() {
    const form = this.$scope.formUserPersonal;
    if (form.$invalid || !form.$dirty) {
      return false;
    }
    // acceptReceiveEmail: true
    // acceptReceiveSms: false

    this.userData.education = form.education.$modelValue.id;
    this.userData.familyIncome = form.familyIncome.$modelValue.id;
    this.userData.frequencyGame = form.gameFrequency.$modelValue.id;
    this.userData.interest = form.interest.$modelValue.id;
    this.userData.marritialStatus = form.marritialStatus.$modelValue.id;
    this.userData.nationality = form.nationality.$modelValue.id;
    this.userData.profession = form.profession.$modelValue.id;
    this.userData.sex = form.genre.$modelValue.id;

    this.save(this.userData).then((data) => {
      if (data) {
        this.$log.log('FormPersonalData - > Success');
      }
    });
  }

  onCancel() {
    if (typeof this.onCancel === 'function') {
      this.cancel(this.userData, this.userId);
    }
    this.userData = angular.copy(this.personalData);
  }
}
PersonalFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class PersonalFormComponent {
  constructor() {
    this.template = require("./personal-form.view.html");
    this.controller = PersonalFormController;
    this.bindings = {
        userId: "<",
        userImg: "<",
        personalData: "<",
        save: "<",
        cancel: "<"
    }
  }
}

export { PersonalFormComponent };
