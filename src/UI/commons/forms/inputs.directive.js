/*global $*/


function InputBehavior ($filter) {

    return {
        restrict: "A",
        require: "ngModel",
        scope: {
            compareWith: '=?',
            inputType: '@'
        },

        link: function(scope, element, attr, ngModel) {
            var elem = $(element);

            const elementKeyUp = () => {
                let value = elem.val();
                switch (scope.inputType) {
                    case 'cpf':
                        elem.val($filter('formatCpf')(value));
                        break;
                    
                    case 'cep':
                        elem.val($filter('formatCep')(value));
                        break;
        
                    case 'date':
                        elem.val($filter('formatDate')(value));
                        break;
        
                    case 'number':
                        elem.val($filter('onlyNumbers')(value));
                        break;
        
                    case 'tel':
                        elem.val($filter('formatTel')(value));
                        break;
                
                    default:
                        break;
                }
            };

            const elementInteraction = (ev) => {
                if (ngModel.$invalid && (ngModel.$dirty || ngModel.$touched) && (ev.type == 'focus')) {
                    elem.parent().addClass('showInputMsg');
                }
                else {
                    elem.parent().removeClass('showInputMsg');
                }
            }

            const initBehavior = () => {

                switch (scope.inputType) {
                    case 'email':
                        ngModel.$validators.email = (modelValue, viewValue) => $filter('isEmail')(modelValue || viewValue);
                        break;
                    
                    case 'cep':
                        ngModel.$validators.cep = (modelValue, viewValue) => $filter('isCep')(modelValue || viewValue);
                        ngModel.$parsers.push((value) => $filter('onlyNumbers')(value));
                        ngModel.$formatters.push((value) => $filter('formatCep')(value));
                        break;
                    
                    case 'cpf':
                        ngModel.$validators.cpf = (modelValue, viewValue) => $filter('isCpf')(modelValue || viewValue);
                        ngModel.$parsers.push((value) => $filter('onlyNumbers')(value));
                        ngModel.$formatters.push((value) => $filter('formatCpf')(value));
                        break;
                    
                    case 'date':
                        ngModel.$validators.date = (modelValue, viewValue) => {
                            let value = modelValue || viewValue;
                            let d = new Date($filter('dateToSql')(value));
                            return (value && $filter('onlyNumbers')(value).length === 8) && (d instanceof Date && !isNaN(d));
                        };

                        ngModel.$parsers.push((value) => {
                            if (value && $filter('onlyNumbers')(value).length === 8) {
                                return $filter('dateToSql')(value);
                            }
                            return value;
                        });

                        ngModel.$formatters.push((value) => $filter('date')(value, $filter('translate')('globals.dates.short')));
                        break;
                    
                    case 'number':
                        ngModel.$parsers.push((value) => (value) ? $filter('onlyNumbers')(value.toString()) : value);
                        ngModel.$formatters.push((value) => (value) ? $filter('onlyNumbers')(value.toString()) : value);
                        break;

                    case 'tel':
                        ngModel.$validators.tel = (modelValue, viewValue) => $filter('isTel')(modelValue || viewValue);
                        ngModel.$parsers.push((value) => $filter('onlyNumbers')(value));
                        ngModel.$formatters.push((value) => $filter('formatTel')(value));
                        break;
                
                    default:
                        break;
                }
            }

            scope.$watch('compareWith', () => {
                if (scope.compareWith) {
                    ngModel.$validators.compare = (modelValue, viewValue) => {
                        let value = modelValue || viewValue;
                        return value == scope.compareWith;
                    };
                    ngModel.$setValidity("compare", (ngModel.$modelValue || ngModel.$viewValue) == scope.compareWith);
                }
            });

            scope.$watch('inputType', (newValue, oldValue) => {
                if (oldValue && newValue != oldValue) {
                    ngModel.$error = {};
                    delete ngModel.$validators[oldValue];
                    ngModel.$parsers = [];
                    ngModel.$formatters = [];
                    ngModel.$setViewValue('');
                    ngModel.$render();

                    ngModel.$setValidity(oldValue, true);

                    initBehavior();
                }
            })

            elem.on('keydown focus blur', elementInteraction);

            elem.on('keyup', elementKeyUp);

            initBehavior();

        }
    };
}
InputBehavior.$inject = ["$filter", "$log"];

export { InputBehavior };