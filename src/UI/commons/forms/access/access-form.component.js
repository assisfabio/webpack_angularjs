"use strict";

class AccessFormController {
  constructor($scope, $rootScope, formsService, $log) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.formsService = formsService;
    this.$log = $log;
  }

  $onInit(){
    
  }

  save() {
    const form = this.$scope.formAccess;
    
    if (form.$invalid || !form.$dirty) {
      return false;
    }

    this.$log.log(form);
    if (typeof this.onSave === 'function') {
      this.onSave(this.userInfo, {
        userCpf: form.accessUserCpf.$modelValue,
        userName: form.accessUserName.$modelValue,
        sector: form.accessSector.$modelValue
      });
    }
  }
}
AccessFormController.$inject = ["$scope", "$rootScope", "formsService", "$log"];

class AccessFormComponent {
  constructor() {
    this.template = require("./access-form.view.html");
    this.controller = AccessFormController;
    this.bindings = {
        sectors: "<",
        userInfo: "<",
        onSave: "<"
    }
  }
}

export { AccessFormComponent };