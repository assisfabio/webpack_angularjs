class SocialController {
  constructor(navigationsService, $log) {
    this.navigationsService = navigationsService;
    this.$log = $log;
  }

  $onInit() {
    this.getItems();
  }

  getItems() {
    this.navigationsService.getMenuSocial()
      .then((data) => {
        this.menu = data;
      });
  }
}
SocialController.$inject = ["navigationsService", "$log"];

export { SocialController };
