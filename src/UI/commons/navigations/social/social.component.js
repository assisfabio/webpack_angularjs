import './social.scss';
import { SocialController } from "./social.controller";

class SocialComponent {
  constructor() {
    this.controller = SocialController;
    this.restrict = 'E',
    this.template = require("./social.view.html");
    this.transclude = true;
    this.bindings = {
      cssId: '<',
      type: "<",
      showIcons: "@"
    };
  }
}

export { SocialComponent };
