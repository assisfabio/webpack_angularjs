let $ctrl;
class NavigationsService {
  constructor(APP_CONFIG, $http, messageService, translateService, $log) {
    $ctrl = this;
    $ctrl.apiUrl = APP_CONFIG.API.URL;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
    $ctrl.translateService = translateService;
  }

  getMenu(type) {
    //return $ctrl.$http.get(`${$ctrl.apiUrl}/layout/menu/${type}`)
    return $ctrl.$http.get(`mocks/menu-${$ctrl.translateService.get.language()}.json?type=${type}`)
      .then(
        $ctrl.messageService.returnResponseFirstItem,
        $ctrl.messageService.errorHandler
      );
  }

  getMenuSocial() {
    //return $ctrl.$http.get(`${$ctrl.apiUrl}/layout/menu/${type}`)
    return $ctrl.$http.get(`mocks/menu-social.json`)
      .then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }
}

NavigationsService.$inject = ["APP_CONFIG", "$http", "messageService", "translateService", "$log"];

export { NavigationsService };
