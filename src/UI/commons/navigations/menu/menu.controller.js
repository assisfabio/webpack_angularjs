import angular from 'angular';
class MenuController {
  constructor($state, $rootScope, $log, navigationsService, loginServices, $transitions, $element) {
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.$log = $log;
    this.navigationsService = navigationsService;
    this.loginServices = loginServices;
    this.$transitions = $transitions;
    this.$element = $element;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });

    this.$rootScope.$on('setlang', () => {
      this.getItems();
    });

    this.$transitions.onStart({}, () => {
      let menuBtn = this.$element[0].querySelector('#toggle-'+this.cssId);
      if (menuBtn && !angular.element(menuBtn).hasClass('collapsed')) {
        menuBtn.click();
      }
    });
  }

  getItems() {
    this.navigationsService.getMenu(this.type)
      .then((data) => {
        this.menu = data;
      });
  }

  getMenuLink(stateName) {
    this.$state.go(stateName, {});
  }
}
MenuController.$inject = ["$state", "$rootScope", "$log", "navigationsService", "loginServices", "$transitions", "$element"];

export { MenuController };
