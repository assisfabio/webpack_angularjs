import './menu.scss';
import { MenuController } from "./menu.controller";

class MenuComponent {
  constructor() {
    this.controller = MenuController;
    this.restrict = 'E',
    this.template = require("./menu.view.html");
    this.transclude = true;
    this.bindings = {
      cssId: '<',
      type: "<",
      showIcons: "@"
    };
  }
}

export { MenuComponent };
