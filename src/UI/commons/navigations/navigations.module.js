"use strict";
import angular from "angular";

import { MenuComponent } from "./menu/menu.component";
import { SocialComponent } from "./social/social.component";
import { NavigationsService } from "./navigations.services";

const MENU_MODULE = angular
    .module("component.navigations", [])
    .service('navigationsService', NavigationsService)
    .component('f2pMenu', new MenuComponent())
    .component('f2pSocial', new SocialComponent());

export { MENU_MODULE };
