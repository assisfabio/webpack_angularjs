import './header.scss';

class HeaderController {
  constructor($log){
    this.$log = $log;
  }

  $onInit(){
  }
}
HeaderController.$inject = ["$log"];

class HeaderComponent {
  constructor() {
    this.controller = HeaderController;
    this.restrict = 'E',
    this.template = require("./header.view.html");
    this.transclude = true;
    this.bindings = {
      showLanguage: "<"
    };
  }
}

export { HeaderComponent };
