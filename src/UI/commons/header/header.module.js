"use strict";
import angular from "angular";

import { HeaderComponent } from "./header.component";

const HEADER_MODULE = angular
    .module("component.header", [])
    .component('f2pHeader', new HeaderComponent());

export { HEADER_MODULE };
