"use strict";
import angular from "angular";

import { selectLanguageComponent } from "./select.component";

const TRANSLATE_MODULE = angular
    .module("component.translate", [])
    .component('f2pSelectLang', new selectLanguageComponent());

export { TRANSLATE_MODULE };
