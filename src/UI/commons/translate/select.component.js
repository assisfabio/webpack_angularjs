import "../../i18n/languages";
import "./select-language.scss";

class selectLanguageController {
  constructor($rootScope,translateService) {
    this.$rootScope = $rootScope;
    this.translateService = translateService;
    this.registeredLangs = null;
    this.langsTables = {
      "en": "English",
      "en-US": "English - US",
      "pt": "Português - PT",
      "pt-BR": "Português - BR",
      "de": "Deutch"
    };
    this.activeLang = null;
  }

  $onInit() {
    this.getLangs();
  }

  getLangs() {
    this.activeLang = this.translateService.get.language();
    this.registeredLangs = this.translateService.get.languages();
  }

  setLanguage(lang) {
    this.translateService.set.language(lang);
    this.activeLang = this.translateService.get.language();
    this.$rootScope.$broadcast('setlang');
  }
}

selectLanguageController.$inject = ["$rootScope", "translateService"];

class selectLanguageComponent {
  constructor() {
    this.template = require("./select-language.view.html");
    this.controller = selectLanguageController;
    this.transclude = true;
  }
}

export { selectLanguageComponent };
