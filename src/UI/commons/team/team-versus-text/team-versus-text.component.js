import "./team-versus-text.scss";

class TeamVersusTextComponent {
  constructor() {
    this.restrict = 'E',
    this.template = require("./team-versus-text.view.html");
    this.transclude = true;
    this.bindings = {
      homeTeam: "<",
      visitorTeam: "<",
      cssClass: "<"
    }
  }
}

export { TeamVersusTextComponent };
