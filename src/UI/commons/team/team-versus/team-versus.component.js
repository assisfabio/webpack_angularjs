import "./team-versus.scss";

class TeamVersusComponent {
  constructor() {
    this.restrict = 'E',
    this.template = require("./team-versus.view.html");
    this.transclude = true;
    this.bindings = {
      homeTeam: "<",
      visitorTeam: "<",
      cssClass: "<"
    }
  }
}

export { TeamVersusComponent };
