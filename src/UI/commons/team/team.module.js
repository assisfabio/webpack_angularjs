"use strict";
import angular from "angular";
import { TeamVersusComponent } from "./team-versus/team-versus.component";
import { TeamVersusTextComponent } from "./team-versus-text/team-versus-text.component";

const TEAM_MODULE = angular
    .module("component.team", [])
    .component("f2pTeamVersus", new TeamVersusComponent())
    .component("f2pTeamVersusText", new TeamVersusTextComponent());

export { TEAM_MODULE };
