import angular from "angular";

class UserPhotoService {

    constructor($http, $rootScope) {
        this.$http = $http;
        this.$rootScope = $rootScope;
    }

    setPhoto(cliente, data) {
        const fd = new FormData();
        fd.append('foto', data);

        return this.$http({
            method: 'POST',
            url: `api/photo.php?cliente=${cliente}`,
            data: fd,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then((response) => {
            this.$rootScope.$emit('update-photo', response.data.url + "?" + new Date().getTime());
        });
    }
}
UserPhotoService.$inject = ["$http", "$rootScope"];

export { UserPhotoService };