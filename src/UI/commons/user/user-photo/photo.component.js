import "./photo.scss";
import { PhotoController } from "./photo.controller";

class PhotoComponent {
  constructor() {
    this.controller = PhotoController;
    this.restrict = 'E',
    this.template = require("./photo.view.html");
    this.transclude = true;
    this.bindings = {
      userId: "<",
      userPhoto: "<"
    };
  }
}

export { PhotoComponent }