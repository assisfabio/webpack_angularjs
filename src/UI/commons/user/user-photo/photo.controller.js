/*global webkitMediaStream $ window*/
/*eslint no-undef: ["error", { "typeof": true }] */
import Webcam from "webcamjs";

let $ctrl;
class PhotoController {

    constructor($log, $scope, $rootScope, photoService, $timeout, $filter, $window, IS_MOBILE){
        $ctrl = this;
        $ctrl.$scope = $scope;
        $ctrl.$rootScope = $rootScope;
        $ctrl.$log = $log;
        $ctrl.photoService = photoService;
        $ctrl.$timeout = $timeout;
        $ctrl.$filter = $filter;
        $ctrl.$window = $window;
        $ctrl.Webcam = Webcam;
        $ctrl.isMobile = IS_MOBILE;
    }

    $onInit() {
        $ctrl.$log.log('ismobile', $ctrl.isMobile);
        $ctrl.mobilecheck = $ctrl.isMobile;
        $ctrl.cameraLoaded = false;

        if (!$ctrl.mobilecheck) {
            $ctrl.Webcam.set({
                width: '493',
                height: '370',
                dest_width: '493',
                dest_height: '370',
                image_format: 'jpeg',
                jpeg_quality: '100',
                force_flash: true,
                flip_horiz: true,
                fps: 60
            });
            $ctrl.Webcam.on('live', function () {
                $ctrl.cameraLoaded = true;
            });
        }

        $ctrl.inputImage = null;
        $ctrl.outputImage = null;
        $ctrl.showWebcam = false;
        $ctrl.rotation = 10;

        $ctrl._video = null;
        $ctrl.patData = null;

        $ctrl.showDemos = false;
        $ctrl.patOpts = {x: 0, y: 0, w: 25, h: 25};

        $ctrl.myChannel = {
            // the fields below are all optional
            videoHeight: '370',
            videoWidth: '493',
            video: null // Will reference the video element on success
        };

        $ctrl.webcamError = false;

        $ctrl.$rootScope.$on('update-photo', (ev, args) => {
            $ctrl.userPhoto = args;
            $('#userPhotoModal').modal('hide');
        })

        $ctrl.$width = $($ctrl.$window).width()

        const base = ($ctrl.$width <= 500) ? 300 : 498;

        $ctrl.boundry = {w: base, h: base/4*3};
        $ctrl.viewport = {w: base*0.6626, h: base*0.6626};
    }

    camLive(){
        $ctrl.startedCamera = true;
        $ctrl.showWebcam = true;
    }
    
    rotateImageL() {
      // rotateBase64Image($ctrl.inputImage);
      $ctrl.rotation -= 90; // 90 deg rotation
    }

    rotateImageR() {
      // rotateBase64Image($ctrl.inputImage);
      $ctrl.rotation += 90; // 90 deg rotation
    }


    onError(err) {
        $ctrl.$timeout(() => {
            $ctrl.$scope.$apply(() => {
                $ctrl.webcamError = err;
                $ctrl.startedCamera = true;
            });
        });
    }

    myLoaded() {
        $ctrl.stopCamera();
    }

    onSuccess() {
        // The video element contains the captured camera data
        $ctrl.cameraLoaded = true;
        $ctrl._video = $ctrl.myChannel.video;
        $ctrl.$timeout(() => {
            $ctrl.$scope.$apply(() => {
                $ctrl.patOpts.w = $ctrl._video.width;
                $ctrl.patOpts.h = $ctrl._video.height;
            });
        });
    }

    /*onStream(stream) {
        // You could do something manually with the stream.
        $ctrl.$log.log(stream);
    }*/

    startCamera() {
        // starting camera
        $ctrl.showWebcam = true;
        $ctrl.cameraLoaded = false;
        $ctrl.$rootScope.$broadcast('START_WEBCAM');
    }

    stopCamera() {
        // stopping camera
        var MediaStream = window.MediaStream;
        if (typeof MediaStream === 'undefined' && typeof webkitMediaStream !== 'undefined') {
            MediaStream = webkitMediaStream;
        }
        
        if (typeof MediaStream !== 'undefined' && !('stop' in MediaStream.prototype)) {
            MediaStream.prototype.stop = function () {
                this.getAudioTracks().forEach((track) => {
                    track.stop();
                });
                
                this.getVideoTracks().forEach((track) => {
                    track.stop();
                });
            };
        }
        // $ctrl.startedCamera = false;
        $ctrl.showWebcam = false;
    }

    /**
    * Make a snapshot of the camera data and show it in another canvas.
    */
    makeSnapshot() {
        if ($ctrl.showWebcam === true) {
            if ($ctrl._video) {
                const patCanvas = document.querySelector('#snapshot');
                
                if (!patCanvas) {
                    return;
                }
                patCanvas.width = $ctrl._video.width;
                patCanvas.height = $ctrl._video.height;
                
                const ctxPat = patCanvas.getContext('2d');
                const idata = $ctrl.getVideoData($ctrl.patOpts.x, $ctrl.patOpts.y, $ctrl.patOpts.w, $ctrl.patOpts.h);
                
                ctxPat.putImageData(idata, 0, 0);

                $ctrl.sendSnapshotToServer(patCanvas.toDataURL('image/jpeg', 1.0));
                $ctrl.patData = idata;
            }
            $ctrl.stopCamera();
        }
        else {
            $ctrl.startCamera();
        }
    }

    getVideoData (x, y, w, h) {
        const hiddenCanvas = document.createElement('canvas');
        hiddenCanvas.width = $ctrl._video.width;
        hiddenCanvas.height = $ctrl._video.height;
        
        const ctx = hiddenCanvas.getContext('2d');
        ctx.drawImage($ctrl._video, 0, 0, $ctrl._video.width, $ctrl._video.height);
        
        return ctx.getImageData(x, y, w, h);
    }

    /**
    * This function could be used to send the image data
    * to a backend server that expects base64 encoded images.
    *
    * In this example, we simply store it in the scope for display.
    */
    sendSnapshotToServer(imgBase64) {
        $ctrl.inputImage = imgBase64;
    }
    
    save () {
        if ($ctrl.outputImage) {
            $ctrl.photoService.setPhoto($ctrl.userId, $ctrl.$filter('toBlob')($ctrl.outputImage));
        }
    }
}

PhotoController.$inject = ["$log", "$scope", "$rootScope", "photoService", "$timeout", "$filter", "$window", "IS_MOBILE"];
export { PhotoController };
