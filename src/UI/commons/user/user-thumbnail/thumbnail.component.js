import './thumbnail.scss';

class ThumbnailController {
  constructor() {
  }

  $onInit(){
    this.defaultImg = (this.defaultImg) ? this.defaultImg : 'user-unknow.png';
  }
}
// ThumbnailController.$inject = [];

/**
 * @example
 * <f2p-user-thumbnail
 *    data-has-img="boolean"
 *    data-img="string"
 *    data-default-img="string"
 *    data-alt="string"
 * ></f2p-user-thumbnail>
 */
class ThumbnailComponent {
  constructor() {
    this.controller = ThumbnailController;
    this.restrict = 'E',
    this.template = require("./thumbnail.view.html");
    this.transclude = true;
    this.bindings = {
      hasImg: "<",
      img: "<",
      defaultImg :"<",
      alt: "<"
    };
  }
}

export { ThumbnailComponent };
