"use strict";
import angular from "angular";
// import "webcamjs";
// import "ng-camera";
import "webcam-directive";
// import "../ng-croppie/ng-croppie";
import { UserService } from "./user.services";
import { ToastComponent } from "./toast/toast.component";
import { ThumbnailComponent } from "./user-thumbnail/thumbnail.component";
import { PhotoComponent } from "./user-photo/photo.component";
import { UserPhotoService } from "./user-photo/photo.service";
import { FileSelectDirective } from "./user-photo/file-select.directive";

const USER_MODULE = angular
    .module("userModule", ["webcam", "ngCroppie"])
    .service("userService", UserService)
    .service("photoService", UserPhotoService)
    .directive("fileSelect", FileSelectDirective)
    .component('f2pUserToast', new ToastComponent())
    .component('f2pUserThumbnail', new ThumbnailComponent())
    .component('f2pUserPhoto', new PhotoComponent());

export { USER_MODULE };
