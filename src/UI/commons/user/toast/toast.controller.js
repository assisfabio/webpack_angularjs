class ToastController {
  constructor($rootScope, userService, loginServices, $log) {
    this.$rootScope = $rootScope;
    this.userService = userService;
    this.loginServices = loginServices;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.getItems();
    }

    this.$rootScope.$on('loggedin', () => {
      this.getItems();
    });

    this.$rootScope.$on('update-photo', () => {
      this.getItems();
    });

    this.$rootScope.$on('setlang', () => {
      this.getItems();
    });
  }

  getItems() {
    this.userService.getData()
      .then((data) => {
        this.user = data.userMenu;
      });
  }

  logout() {
    this.loginServices.logout();
  }
}
ToastController.$inject = ["$rootScope", "userService", "loginServices", "$log"];

export { ToastController };
