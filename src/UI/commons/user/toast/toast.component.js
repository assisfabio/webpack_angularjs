import './toast.scss';
import { ToastController } from "./toast.controller";

class ToastComponent {
  constructor() {
    this.controller = ToastController;
    this.restrict = 'E',
    this.template = require("./toast.view.html");
    this.transclude = true;
  }
}

export { ToastComponent };
