let _this;
class UserService {
  constructor(APP_CONFIG, $http, messageService, $log) {
    _this = this;
    _this.apiUrl = APP_CONFIG.API.URL;
    _this.$http = $http;
    _this.$log = $log;
    _this.messageService = messageService;
  }

  getData() {
    //return _this.$http.get(`${_this.apiUrl}/layout/menu/main-header`)
    return _this.$http.get('api/main-menu.php')
      .then(
        this.messageService.returnResponseFirstItem,
        this.messageService.errorHandler
      );
  }
}

UserService.$inject = ["APP_CONFIG", "$http", "messageService", "$log"];

export { UserService };
