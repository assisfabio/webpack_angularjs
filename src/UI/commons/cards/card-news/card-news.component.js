import "./card-news.scss";

class CardNewsController {
  constructor($rootScope, $log) {
    this.$rootScope = $rootScope;
    this.$log = $log;
  }

  $onInit() {
  }
}
CardNewsController.$inject = ["$rootScope", "$log"];

class CardNewsComponent {
  constructor() {
    this.controller = CardNewsController;
    this.restrict = 'E',
    this.template = require("./card-news.view.html");
    this.transclude = true;
    this.bindings = {
        item: "<",
        cssClass: "<"
    }
  }
}

export { CardNewsComponent };
