import "./card-product.scss";

class CardProductController {
  constructor($rootScope, $log, cartServices) {
    this.$rootScope = $rootScope;
    this.$log = $log;
    this.cartServices = cartServices;
  }

  $onInit() {
  }

  addToCart(item) {
    this.cartServices.setItem(item);
  }
}
CardProductController.$inject = ["$rootScope", "$log", "cartServices"];

class CardProductComponent {
  constructor() {
    this.controller = CardProductController;
    this.restrict = 'E',
    this.template = require("./card-product.view.html");
    this.transclude = true;
    this.bindings = {
        product: "<",
        cssClass: "<",
        id: "<"
    }
  }
}

export { CardProductComponent };
