import "./card-service.scss";

class CardServiceController {
  constructor($rootScope, $log, cartServices) {
    this.$rootScope = $rootScope;
    this.$log = $log;
    this.cartServices = cartServices;
  }

  $onInit() {
  }

  addToCart(item) {
    this.cartServices.setItem(item);
  }
}
CardServiceController.$inject = ["$rootScope", "$log", "cartServices"];

class CardServiceComponent {
  constructor() {
    this.controller = CardServiceController;
    this.restrict = 'E',
    this.template = require("./card-service.view.html");
    this.transclude = true;
    this.bindings = {
        service: "<",
        cssClass: "<",
        id: "<"
    }
  }
}

export { CardServiceComponent };
