"use strict";
import angular from "angular";
import { CardProductComponent } from "./card-product/card-product.component";
import { CardGameComponent } from "./card-game/card-game.component";
import { CardServiceComponent } from "./card-service/card-service.component";
import { CardNewsComponent } from "./card-news/card-news.component";

const CARDS_MODULE = angular
    .module("component.cards", [])
    .component("f2pCardProduct", new CardProductComponent())
    .component("f2pCardGame", new CardGameComponent())
    .component("f2pCardService", new CardServiceComponent())
    .component("f2pCardNews", new CardNewsComponent());

export { CARDS_MODULE };
