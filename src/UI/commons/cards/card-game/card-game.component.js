import "./card-game.scss";

class CardGameController {
  constructor($rootScope, $log) {
    this.$rootScope = $rootScope;
    this.$log = $log;
  }

  $onInit() {
  }

  addToCart(item) {
    this.cartServices.setItem(item);
  }
}
CardGameController.$inject = ["$rootScope", "$log"];

class CardGameComponent {
  constructor() {
    this.controller = CardGameController;
    this.restrict = 'E',
    this.template = require("./card-game.view.html");
    this.transclude = true;
    this.bindings = {
        game: "<",
        cssClass: "<",
        id: "<"
    }
  }
}

export { CardGameComponent };
