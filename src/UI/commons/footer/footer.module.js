"use strict";
import angular from "angular";

import { FooterComponent } from "./footer.component";

const FOOTER_MODULE = angular
    .module("component.footer", [])
    .component('f2pFooter', new FooterComponent());

export { FOOTER_MODULE };
