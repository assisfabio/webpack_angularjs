import './footer.scss';

class FooterController {
  constructor($log){
    this.$log = $log;
  }

  $onInit(){
  }
}
FooterController.$inject = ["$log"];

class FooterComponent {
  constructor() {
    this.controller = FooterController;
    this.restrict = 'E',
    this.template = require("./footer.view.html");
    this.transclude = true;
  }
}

export { FooterComponent };
