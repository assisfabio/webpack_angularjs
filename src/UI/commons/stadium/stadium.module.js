"use strict";
import angular from "angular";

import { StadiumInfoComponent } from "./stadium-info/stadium-info.component";

const STADIUM_MODULE = angular
    .module("component.stadium", [])
    .component('stadiumInfo', new StadiumInfoComponent());

export { STADIUM_MODULE };
