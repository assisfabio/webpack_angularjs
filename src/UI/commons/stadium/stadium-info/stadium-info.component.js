import './stadium-info.scss';

class StadiumInfoComponent {
  constructor() {
    this.restrict = 'E',
    this.template = require("./stadium-info.view.html");
    this.transclude = true;
    this.bindings = {
      stadium: "<"
    }
  }
}

export { StadiumInfoComponent };
