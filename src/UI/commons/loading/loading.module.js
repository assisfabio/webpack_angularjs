"use strict";
import angular from "angular";

import { loadingSpinnerComponent } from "./loading.component";
import { LoadingConfig } from "./loading.configs";

const LOADING_MODULE = angular
    .module("component.loading", [])
    .config(LoadingConfig)
    .component('loadingSpinner', new loadingSpinnerComponent());

export { LOADING_MODULE };
