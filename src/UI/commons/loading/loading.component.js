import "./loading-spinner.scss";

class LoadingController {
  constructor($rootScope) {
    this.$rootScope = $rootScope;
  }
}
LoadingController.$inject = ["$rootScope"];


class loadingSpinnerComponent {
  constructor() {
    this.controller = LoadingController;
    this.restrict = 'E',
    this.template = require("./loading.view.html");
    this.transclude = true;
  }
}
export { loadingSpinnerComponent };
