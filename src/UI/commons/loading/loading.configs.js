/**
 * Intercept all calls without ignoreLoading param
 * and add a counter inside a global variable it sets Loading to shows
 *
 * NOTE: APP_CONFIG must be defined as angular.constant.
 *       This constant is needed over entire application
 *       See app.js for more infos
 */
const LoadingConfig = function($httpProvider) {
  function loadingProviders ($q, $rootScope, APP_CONFIG) {
    const api_url = new RegExp(`${APP_CONFIG.API.URL}.*/`);
    return {
      'request': function(config) {
        if (config.url.match(api_url) && !config.ignoreLoading) {
          if (!$rootScope.loading || $rootScope.loading < 0) {
            $rootScope.loading = 0;
          }
          $rootScope.loading++;
        }
        return config || $q.when(config);
      },
      'response': function(response) {
        if (response.config.url.match(api_url) && !response.config.ignoreLoading) {
          if ($rootScope.loading && $rootScope.loading > 0) {
            $rootScope.loading--;
          }
        }
        return response || $q.when(response);
      },
      'responseError': function(rejection) {
        if (rejection.config.url.match(api_url) && !rejection.config.ignoreLoading) {
          if ($rootScope.loading && $rootScope.loading > 0) {
            $rootScope.loading--;
          }
        }
        if (rejection.status <= 0) {
          //uiMessageService.show.message(rejection);
        }
        return $q.reject(rejection);
      }
    };
  }
  $httpProvider.interceptors.push(['$q', '$rootScope', 'APP_CONFIG', loadingProviders]);
}
LoadingConfig.$inject = ["$httpProvider"];

export { LoadingConfig };
