"use strict";
import angular from "angular";

import { NextGameComponent } from "./next-game/next-game.component";
import { NextGameService } from "./next-game.services";

const NEXTGAME_MODULE = angular
    .module("component.nextGame", [])
    .service('nextGameService', NextGameService)
    .component('f2pNextGame', new NextGameComponent());

export { NEXTGAME_MODULE };
