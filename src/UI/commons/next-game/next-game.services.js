let $ctrl;
class NextGameService {
  constructor(APP_CONFIG, $http, messageService, translateService, $log) {
    $ctrl = this;
    $ctrl.apiUrl = APP_CONFIG.API.URL;
    $ctrl.$http = $http;
    $ctrl.$log = $log;
    $ctrl.messageService = messageService;
    $ctrl.translateService = translateService;
  }

  getGames() {
    //return $ctrl.$http.get(`${$ctrl.apiUrl}/layout/menu/${type}`)
    return $ctrl.$http.get(`mocks/next-game-${$ctrl.translateService.get.language()}.json`)
      .then(
        $ctrl.messageService.returnResponseMsgSaida,
        $ctrl.messageService.errorHandler
      );
  }

  getNextGame() {
    //return $ctrl.$http.get(`${$ctrl.apiUrl}/layout/menu/${type}`)
    return $ctrl.$http.get(`mocks/next-game-${$ctrl.translateService.get.language()}.json`)
      .then(
        $ctrl.messageService.returnResponseFirstItem,
        $ctrl.messageService.errorHandler
      );
  }
}

NextGameService.$inject = ["APP_CONFIG", "$http", "messageService", "translateService", "$log"];

export { NextGameService };
