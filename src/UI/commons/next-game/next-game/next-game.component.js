import './next-game.scss';

class NextGameController {
  constructor($state, $rootScope, nextGameService, loginServices, $log) {
    this.$state = $state;
    this.$rootScope = $rootScope;
    this.nextGameService = nextGameService;
    this.loginServices = loginServices;
    this.$log = $log;
  }

  $onInit() {
    if (this.loginServices.isLogged()) {
      this.init();
    }
    
    this.$rootScope.$on('loggedin', () => {
      this.init();
    });

    this.$rootScope.$on('setlang', () => {
      this.init();
    });
  }

  init(){
    this.getItem();
  }

  getItem() {
    this.nextGameService.getNextGame()
      .then((data) => {
        this.game = data;
        this.$rootScope.nextGame = this.game;
        this.$rootScope.$emit('next-game', this.game);
      });
  }

  doCheckin() {
    this.$state.go('checkin', {});
  }
}
NextGameController.$inject = ["$state", "$rootScope", "nextGameService", "loginServices", "$log"];

class NextGameComponent {
  constructor() {
    this.controller = NextGameController;
    this.restrict = 'E',
    this.template = require("./next-game.view.html");
    this.transclude = true;
    this.bindings = {
      page: "<"
    }
  }
}

export { NextGameComponent };
