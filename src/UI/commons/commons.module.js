"use strict";
import angular from "angular";

import "./cards/cards.module";
import "./footer/footer.module";
import "./forms/forms.module";
import "./header/header.module";
import "./next-game/next-game.module";
import "./loading/loading.module";
// import "./modals/modals.module";
import "./message/message.module";
import "./navigations/navigations.module";
import "./slides/slides.module";
import "./stadium/stadium.module";
import "./team/team.module";
import "./translate/translate.module";
import "./user/user.module";

const COMMONS_MODULE = angular
    .module("ui.commons", [
      "component.cards",
      "component.footer",
      "component.header",
      "component.loading",
      "component.nextGame",
      "component.message",
      // "component.modals",
      "component.navigations",
      "component.slides",
      "component.stadium",
      "component.team",
      "component.translate",
      "formsModule",
      "userModule"
    ]);

export { COMMONS_MODULE };
