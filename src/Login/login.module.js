"use strict";
import angular from "angular";
import { LoginVerification } from "./login.verification";
import { LoginInterceptor } from "./login.interceptor"
import { LoginController } from "./login.controller";
import { LoginServices } from "./login.services";

import { LoginFormComponent } from "./login-form/login-form.component";
import { JoinFormComponent } from "./join-form/join-form.component";

import "./login.scss";

class LoginComponent {
  constructor() {
    this.template = require("./login.view.html");
    this.controller = LoginController;
  }
}



const LOGIN_MODULE = angular
  .module("loginModule", [])
  .config(LoginInterceptor)
  .run(LoginVerification)
  .service("loginServices", LoginServices)
  .component("f2pLogin", new LoginComponent())
  .component("f2pLoginForm", new LoginFormComponent())
  .component("f2pJoinForm", new JoinFormComponent());

export { LOGIN_MODULE };
