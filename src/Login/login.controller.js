"use strict";

class LoginController {
  constructor ($rootScope, loginServices) {
    this.$rootScope = $rootScope;
    this.loginServices = loginServices;
  }

  $onInit(){
    this.$rootScope.$on('loggedout', () => {
      this.loginServices.logout();
    });
  }

  login(user, pass){
    this.loginServices.login(user, pass);
  }

  logout(){
    this.loginServices.logout();
  }
}
LoginController.$inject = ["$rootScope", "loginServices"];

export { LoginController };
