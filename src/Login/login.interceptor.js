/**
 * Intercept all calls to verify validity of Auth Token
 *
 * NOTE: APP_CONFIG must be defined as angular.constant.
 *       This constant is needed over entire application
 *       See app.js for more infos
 */
const LoginInterceptor = function($httpProvider) {
  function loginProviders ($q, $rootScope, APP_CONFIG) {
    const api_url = new RegExp(`/${APP_CONFIG.API.URL}.*/`);
    return {
      'responseError': function(rejection) {
        if (rejection.config.url.match(api_url)) {
          if (rejection.status === 401) { // Loggedout Status from API, Default is 401
            $rootScope.$broadcast('loggedout');
          }
          else if (rejection.status <= 0) { // No Network Status, Default is >= 0
            $rootScope.$broadcast('no-network');
          }
        }
        return $q.reject(rejection);
      }
    };
  }
  $httpProvider.interceptors.push(['$q', '$rootScope', 'APP_CONFIG', loginProviders]);
}
LoginInterceptor.$inject = ["$httpProvider"];

export { LoginInterceptor };
