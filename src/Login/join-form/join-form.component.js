import "./join-form.scss";

class JoinFormController {
  constructor($scope, $attrs, $log) {
    this.$attrs = $attrs;
    this.$scope = $scope;
    this.$log = $log;
  }

  $onInit() {
    this.state = 'initial';
  }

  requestNewPassword() {
    // this.$log.log(this.user);
    this.state = 'username';
    this.setFeedbackMsg({
      class: 'alert-success',
      msg: 'login.forgot.success'
    });
  }

  setFeedbackMsg(msg) {
    this.feedbackMsg = msg;
  }

  join() {
    const form = this.$scope.formJoin;

    if (form.$invalid) {
      return;
    }
    const data = {};
    this.submit(data);
  }
}
JoinFormController.$inject = ["$scope", "$attrs", "$log"];

class JoinFormComponent {
  constructor() {
    this.template = require("./join-form.view.html");
    this.controller = JoinFormController;
    this.bindings = {
      submit: "&"
    };
  }
}

export { JoinFormComponent };
