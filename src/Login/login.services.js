class LoginServices {
  constructor($http, $rootScope, APP_CONFIG, authServices, messageService) {
    this.$http = $http;
    this.$rootScope = $rootScope;
    this.appConfig = APP_CONFIG;
    this.TOKEN_KEY = APP_CONFIG.AUTH.TOKEN_KEY;
    this.TOKEN_PREFIX = APP_CONFIG.AUTH.TOKEN_PREFIX;
    this.authServices = authServices;
    this.messageService = messageService;
    this.$rootScope.logged = authServices.isReturningUser();

    if (this.$rootScope.logged) {
      this.setLogin()
    }
  }

  /**
   * FIXME Do login with real data
   */
  login(user, pass) {
    this.$http({
      method: `${this.appConfig.endpoints.login.enter.method}`,
      url: `${this.appConfig.endpoints.login.enter.url}`,
      data: {
        "userName": user,
        "password": pass
      }
    }).then((response) => {
      if (response.data && response.data.result === "SUCCESS") {
        this.setLogin(response.data.msgSaida[0].token);
      }
      else {
        this.messageService.errorHandler(response)
      }
    }, this.messageService.errorHandler);
  }

  logout(data) {
    this.$http({
      method: `${this.appConfig.endpoints.login.out.method}`,
      url: `${this.appConfig.endpoints.login.out.url}`,
      data: data
    }).then((response) => {
      if (response.data && response.data.result === "SUCCESS") {
        this.logoutClient();
      }
      else {
        this.messageService.errorHandler(response)
      }
    }, this.messageService.errorHandler);
  }

  logoutClient () {
    this.authServices.removeToken();
    delete this.$http.defaults.headers.common['Authorization']
    this.$rootScope.logged = false;
  }

  isLogged() {
    return this.$rootScope.logged && this.authServices.getToken();
  }

  setLogin(token) {
    if (token) {
      this.authServices.setToken(token);
    }
    this.$rootScope.logged = true;
    this.$http.defaults.headers.common['Authorization'] = `${this.TOKEN_PREFIX} ${this.authServices.getToken()}`;
    this.$rootScope.$broadcast('loggedin');
  }
}
LoginServices.$inject = ["$http", "$rootScope", "APP_CONFIG", "authServices", "messageService"];

export { LoginServices };
