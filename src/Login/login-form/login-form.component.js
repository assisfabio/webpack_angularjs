import "./login-form.scss";

class LoginFormController {
  constructor($scope, $attrs, $log) {
    this.$attrs = $attrs;
    this.$scope = $scope;
    this.$log = $log;

    // this.$log.log($scope, this)
  }

  $onInit() {
    this.state = 'username';
  }

  login() {
    const form = this.$scope.formLogin;

    if (form.$invalid) {
      return;
    }
    const data = {user: this.user, pass: this.pass};
    this.submit(data);
  }

  verifyUsername() {
    /**
     * TODO
     * 1. Chamar endpoint para verificar se o CPF informado já é cliente
     * 2. Abrir o state de senha ou o de cadastro de usuário
     **/
    // this.$log.log(this.user);
    this.state = 'password';
  }

  requestNewPassword() {
    // this.$log.log(this.user);
    this.state = 'username';
    this.setFeedbackMsg({
      class: 'alert-success',
      msg: 'login.forgot.success'
    });
  }

  setFeedbackMsg(msg) {
    this.feedbackMsg = msg;
  }

  join() {
    this.state = 'username';
  }
}
LoginFormController.$inject = ["$scope", "$attrs", "$log"];

class LoginFormComponent {
  constructor() {
    this.template = require("./login-form.view.html");
    this.controller = LoginFormController;
    this.bindings = {
      submit: "&"
    };
  }
}

export { LoginFormComponent };
