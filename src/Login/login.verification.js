function LoginVerification (loginServices, $transitions) {
    console.log("LoginVerification");
    const verifyLogin = () => {
        if (!loginServices.isLogged()) {
            loginServices.logoutClient()
        }
    };
    $transitions.onBefore({}, verifyLogin);

    verifyLogin();
}
LoginVerification.$inject = ["loginServices", "$transitions"];

export { LoginVerification };