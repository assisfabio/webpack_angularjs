:heart_eyes:

# AnguPack
Site e-Commerce para Clube de Sócios

## Estrutura do Projeto
```
project-root
├── Assets                              -> Diretório com os Recursos modificáveis

├── src
│   ├── Auth                            -> Módulo com os itens de Autenticação

│   ├── Cart                            -> Módulo com os itens do Carrinho de Compras

│   ├── core
│   │   ├── bootstrap.js                -> Quickstart da Aplicação

│   ├── Login                           -> Módulo de Login

│   ├── pages                           -> Páginas internas

│   │   ├── page-directory              -> Diretorio de cada Página ou Grupo de Páginas
│   │   │   ├── view                    -> Diretorio da Visualização
│   │   │   │   ├── view.module.js      
│   │   │   │   ├── view.view.html
│   │   │   │   ├── view.scss
│   │   │   ├── page.module.js          -> Configuração/Definição do Módulo
│   │   │   ├── page.routing.js         -> Configuração das Rotas
│   │   │   ├── page.service.js         -> Serviços do Módulo
│   │   │   ├── page.state.js           -> Definição dos Estados (urls) de cada sub-página (view)

│   ├── UI                              -> Diretório com os itens da UI
│   │   ├── commons                     -> Componentes comuns (header, footer, cards, slides, etc)
│   │   ├── filters                     -> Filtros de manipulação
│   │   ├── i18n                        -> Módulo de Idiomas
│   │   ├── styles                      -> Configurações globais de Estilo
│   │   ├── ui.module.js                -> Inicializador do módulo de UI

│   ├── app.js                          -> Arquivo com a configuração inicial da Aplicação
│   ├── app.state.interceptor.js        -> Interceptor que faz a marcação da Rota Atual
│   ├── app.storage.js                  -> Configuração do LocaStorage
│   ├── app.style.js                    -> Estilo global do Site
│   ├── app.sw.js                       -> Configuração do ServiceWorker
│   ├── index.html                      -> Arquivo html raiz

├── _custom.scss                        -> Temificação (Override) das variaveis do Bootstrap - Opcional
```

## Compreendendo o arquivo `/app.config.js`
O arquivo `app.config.js` contém algumas configurações extremamente importantes para o Projeto e ambiente de desenvolvimento. É neste arquivo que está configurado o "seletor" da API, utilizado para interceptar o controle dos Loadings das requisições. Também neste arquivo, estão contidos todos os endereços de chamadas para os endpoints da RestAPI.

## Compreendendo o arquivo `/_custom.scss`
O arquivo `_custom.scss` localizado na raiz do projeto, serve para sobre escrever as variáveis do Bootstrap, assim como as variáveis criadas para o Projeto. É através deste arquivo que é possivel modificar as cores, fontes e formas dos elementos do site.

## Compreendendo o diretório `/Assets/`
O diretórios `Assets` contém os arquivos que precisam ser modificados para cada instalação. Dentros deste diretórios estão os arquivos de ícones utilizados pelos sistemas operacionais/navegadores para identificar o seu projeto como um Webapp/PWA

## Rodando em Modo Desevolvimento sem SSL
`npm start`

## Rodando em Modo Desevolvimento com SSL
`npm starthttps`

* Este modo pode gerar alguns conflitos

## Criando uma versão para Publicação
`npm run build`

Os arquivos para publicação serão gerados dentro do diretório `/dist`. Certifique-se dos arquivos gerados e faça um backup dos arquivos atuais antes da publicação.

## TODO
- Recursos de Idioma devem ser recebidos via API, validados por login;
- Corrigir conflitos no BrowserSync;
- Gerar relatório de cobertura em html;
- Incluir biblioteca de ícones usando Fontello;
- Variáveis de ambiente .env