const appConfig = {
    API: {
      URL: "api"
    },
    AUTH: {
      "TOKEN_KEY": "lid",
      "TOKEN_PREFIX": "CustomToken"
    }
}
appConfig.endpoints = {
    account: {
        getUser: {
            method: "get",
            url: `${appConfig.API.URL}/socio-dados.php`
        },
        setPersonalData: {
            method: "put",
            url: `${appConfig.API.URL}/update-socio.php`
        },
        savePassword: {
            method: "put",
            url: `${appConfig.API.URL}/save-password.php`
        },
        saveAddresses: {
            method: "put",
            url: `${appConfig.API.URL}/save-enderecos.php`
        },
        addAddress: {
            method: "post",
            url: `${appConfig.API.URL}/add-endereco.php`
        },
        deleteAddress: {
            method: "delete",
            url: (id) => `${appConfig.API.URL}/delete-endereco.php?id=${id}`
        },
        saveContacts: {
            method: "put",
            url: `${appConfig.API.URL}/save-contatos.php`
        },
        addContact: {
            method: "post",
            url: `${appConfig.API.URL}/add-contato.php`
        },
        deleteContact: {
            method: "delete",
            url: (id) => `${appConfig.API.URL}/delete-contato.php?id=${id}`
        },
    },
    atendimento: {
        getData: {
            method: "GET",
            url: `mocks/atendimento.json`
        }
    },
    beneficios: {
        getData: {
            method: "get",
            url: `mocks/beneficios.json`
        },
        getCities: {
            method: "get",
            url: `mocks/beneficios-municipios.json`
        },
        getCategories: {
            method: "get",
            url: `mocks/beneficios-categorias.json`
        }
    },
    cadeiras: {
        getData: {
            method: "GET",
            url: `mocks/cadeiras.json`
        }
    },
    financial: {
        paymentMethods: {
            method: "GET",
            url: `mocks/metodos-de-pagamento.json`
        }
    },
    jogos: {
        getData: {
            method: "GET",
            url: `mocks/next-game-pt-BR.json`
        }
    },
    login: {
        enter: {
            method: "POST",
            url: `${appConfig.API.URL}/authenticate.php`
        },
        out: {
            method: "PUT",
            url: `${appConfig.API.URL}/logoff.php`
        }
    },
    loja: {
        getData: {
            method: "GET",
            url: `mocks/loja-de-vantagens.json`
        }
    },
    servicos: {
        getData: {
            method: "GET",
            url: `mocks/servicos.json`
        }
    },
};

export { appConfig };