"use strict";

const dotenv = require('dotenv').config();

const path = require("path");
const webpack = require("webpack");
// const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const WriteFilePlugin = require('write-file-webpack-plugin');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

const devServer = {
  contentBase: path.resolve("dist"),
  hot: true,
  host: process.env.host || "localhost",
  port: process.env.PORT || 5000,
  open: true,
  proxy: {
    '/api': {
      target: process.env.API_PROXY_URL,
      //pathRewrite: {'^/api' : ''}
      "secure": false,
      "changeOrigin": true
    }
  },
};

const webpackConfig = (env) => {
  const config = {
    entry: {
      app: [path.resolve("./src/core/bootstrap.js")]
    },
    output: {
      filename: "bundle.js",
      chunkFilename: "[name].chunk.js",
      path: path.join(__dirname, "dist")
    },
    module: {
      rules: [
        // eslint
        {
          enforce: "pre",
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "eslint-loader",
        },

        // babel
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },

        // html
        {
          test: /\.html$/,
          loader: "raw-loader",
          exclude: path.resolve("./src/index.html")
        },
        // Files
        {
          test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
          loader: "file-loader"
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: "style-loader"
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: function () {
                  return [
                    require('precss'),
                    require('autoprefixer')
                  ];
                }
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ]
        },
        {
          test: /\.css$/,

          use: [
            {
              loader: "style-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "file-loader"
            }
          ]
        }
      ]
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }),
      
      new HtmlWebpackPlugin({
        filename: "index.html",
        template: path.resolve("./src/index.html")
      }),

      new webpack.optimize.CommonsChunkPlugin({
        name: "common",
        filename: "common.js",
        minChunks: (module) => {
          // this assumes your vendor imports exist in the node_modules directory
          return module.context && module.context.indexOf("node_modules") !== -1;
        }
      }),

      new CleanWebpackPlugin(["dist/*"]),

      new WriteFilePlugin(),

      new CopyWebpackPlugin(
        [
          { from:'Assets/*', flatten: true},
          { from:'Assets/icons/*', to: 'icons/', flatten: true},
          { from:'Assets/images/*', to: 'images/', flatten: true},
          { from:'Assets/templates/*', to: 'templates/', flatten: true},
          { from:'Assets/mocks/*', to: 'mocks/', flatten: true},
          { from:'Assets/mocked-images/*', to: 'mocked-images/', flatten: true}
        ],
        {
          copyUnmodified: true
        }
      ),

      new ServiceWorkerWebpackPlugin({
        entry: path.join(__dirname, 'Assets/sw.js'),
        excludes: ['**/.*', '**/*.map', '*.html'],
        publicPath: './'
      })
    ]
  };

  if (env && env.dev) {
    config.devServer = devServer;
    config.plugins.push(
      new webpack.NamedModulesPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      // new BrowserSyncPlugin()
    );

    // config.entry.app.push(path.resolve("./browserSync.ws.js"))
  }

  if (env && env.production) {
    config.devtool = "source-map";
    config.plugins.push(
      new UglifyJSPlugin({
        uglifyOptions: {
          warnings: true
        },
        sourceMap: true
      }),
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify("production"),
        "TESTE_VAR": JSON.stringify(process.env.STATIC_HOST)
      }),

      new BundleAnalyzerPlugin()
    );
  }
  else {
    config.plugins.push(
      new webpack.DefinePlugin({
        'TESTE_VAR': JSON.stringify(process.env.STATIC_HOST)
      })
    );
  }

  return config;
};

module.exports = webpackConfig;
