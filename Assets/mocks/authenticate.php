<?php
header('Content-type:application/json');

$data = '{
    "result": "SUCCESS",
    "method": "authenticate",
    "msgSaida": [
        {
            "token": "bd0_admin",
            "menu": {
                "profileImgSrc": "/proxy/data/my-profile-image/bd0_admin",
                "profileName": "USUARIO_TESTE_MENU",
                "header": "Menu Principal",
                "itens": [
                    {
                        "label": "Dashboard",
                        "visualClass": "fa fa-cog",
                        "subItems": []
                    },
                    {
                        "label": "Sócios",
                        "visualClass": "fa fa-cog",
                        "subItems": [
                            {
                                "label": "Pesquisar Sócio",
                                "visualClass": "fa fa-pencil-square-o",
                                "actionLink": "/home/create-form"
                            },
                            {
                                "label": "Cadastrar Sócio",
                                "visualClass": "fa fa fa-comment-o",
                                "actionLink": "/home/246"
                            }
                        ]
                    },
                    {
                        "label": "Cadeiras",
                        "visualClass": "fa fa-cog",
                        "subItems": [
                            {
                                "label": "Geração de Certificados",
                                "visualClass": "fa fa-id-card-o",
                                "actionLink": "/home/242"
                            },
                            {
                                "label": "Gerenciar Certificados",
                                "visualClass": "fa fa-globe",
                                "actionLink": "/home/243"
                            }
                        ]
                    }
                ]
            }
        }
    ],
    "error": []
}
';

echo $data;

?>