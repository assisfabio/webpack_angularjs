<?php
header('Content-type:application/json');
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$data = '{
    "result":"SUCCESS",
    "msgSaida":{
        "id": 5
    },
    "error":[]
}
';

$dataObject = json_decode($data);
$dataObject->msgSaida->type = $request->type->id;
$dataObject->msgSaida->value = $request->value;

echo json_encode($dataObject);