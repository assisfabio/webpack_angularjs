<?php
header('Content-type:application/json');

$data = '{
    "result": "SUCCESS",
    "method": "getMainHeader",
    "msgSaida": [
        {
            "actionBar": [
                {
                    "label": "PARAMETRO",
                    "visualClass": "fa fa fa-comment-o",
                    "actionLink": "/home/246"
                },
                {
                    "label": "USUÁRIO",
                    "visualClass": "fa fa-users",
                    "actionLink": "/home/523"
                }
            ],
            "userMenu": {
                "useImgProfile": true,
                "imgSrc": "api/uploads/blob",
                "imgAlt": "",
                "urlProfile": "/change-my-login",
                "name": "USUARIO_TESTE_MENU",
                "resume": "USUARIO_TESTE_MENU :: ADMINISTRATOR_BD_ADMIN",
                "profileLabel": "MENU PERFIL",
                "signOutLabel": "SAIR",
                "token": "bd0_admin",
                "applicationNameMini": "ADM",
                "applicationName": "BD_ADMIN",
                "actionBar": [
                    {
                        "label": "PARAMETRO",
                        "visualClass": "fa fa fa-comment-o",
                        "actionLink": "/home/246"
                    },
                    {
                        "label": "USUÁRIO",
                        "visualClass": "fa fa-users",
                        "actionLink": "/home/523"
                    }
                ]
            }
        }
    ],
    "error": []
}
';

$dataObject = json_decode($data);

$directory = 'uploads';
$scanned_directory = array_diff(scandir($directory), array('..', '.'));

foreach ($scanned_directory as $file) {
    if (strpos($file, 'blob') === 0) {
        $dataObject->msgSaida[0]->userMenu->imgSrc = "api/uploads/" . $file;
        break;
    }
}

echo json_encode($dataObject);