<?php
header('Content-type:application/json');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$data = '
{
    "result":"SUCCESS",
    "msgSaida": {
        "personalData": {
        },
        "id": 99
    },
    "error":[]
}
';

$dataObject = json_decode($data);

$dataObject->msgSaida->personalData->cpf                = $request->cpf;
$dataObject->msgSaida->personalData->name               = $request->name;
$dataObject->msgSaida->personalData->email              = $request->email;
$dataObject->msgSaida->personalData->email2             = $request->email2;
$dataObject->msgSaida->personalData->rg                 = $request->rg;
$dataObject->msgSaida->personalData->birthDate          = $request->birthDate;
$dataObject->msgSaida->personalData->sex                = $request->sex;
$dataObject->msgSaida->personalData->nationality        = $request->nationality;
$dataObject->msgSaida->personalData->marritialStatus    = $request->marritialStatus;
$dataObject->msgSaida->personalData->profession         = $request->profession;
$dataObject->msgSaida->personalData->education          = $request->education;
$dataObject->msgSaida->personalData->childrenNumber     = $request->childrenNumber;
$dataObject->msgSaida->personalData->familyIncome       = $request->familyIncome;
$dataObject->msgSaida->personalData->frequencyGame      = $request->frequencyGame;
$dataObject->msgSaida->personalData->interest           = $request->interest;
$dataObject->msgSaida->personalData->acceptReceiveEmail = $request->acceptReceiveEmail;
$dataObject->msgSaida->personalData->acceptReceiveSms   = $request->acceptReceiveSms;

echo json_encode($dataObject);