<?php
header('Content-type:application/json');

$data = '
{
    "result":"SUCCESS",
    "msgSaida": {
        "personalData": {
            "cpf": "09876543210",
            "name": "Fulano de Tal 1",
            "email": "fulano1@email.com.br",
            "email2": "fulano1b@email.com.br",
            "rg": "MG11234123",
            "birthDate": "1980-12-31",
            "sex": 99,
            "nationality": 99,
            "marritialStatus": 99,
            "profession": 99,
            "education": 99,
            "childrenNumber": 3,
            "familyIncome": 99,
            "frequencyGame": 99,
            "interest": 99,
            "acceptReceiveEmail": true,
            "acceptReceiveSms": false
        },
        "scheduleData": [
            {
                "id": 1,
                "type": "97",
                "value": "fulano1@email.com.br"
            },
            {
                "id": 2,
                "type": "98",
                "value": "fulano1@emailcomercial.com.br"
            },
            {
                "id": 3,
                "type": "99",
                "value": 3136987532
            },
            {
                "id": 4,
                "type": "100",
                "value": 31986982156
            }
        ],
        "addressData": [
            {
                "id": 1,
                "type": "20202",
                "postalCode": "32654987",
                "address": "Rua do Fulano",
                "number": 100,
                "complement": "Casa",
                "neighborhood": "Centro",
                "city": "Rio de Janeiro",
                "state": "RJ",
                "country": "Brasil"
            },
            {
                "id": 2,
                "type": "223626",
                "postalCode": "32654987",
                "address": "Rua do Fulano Comercial",
                "number": 100,
                "complement": "Casa",
                "neighborhood": "Centro",
                "city": "Rio de Janeiro",
                "state": "RJ",
                "country": "Brasil"
            }
        ],
        "paymentData": {
            "paymentType": 99,
            "cardNumber": "8596545845654895",
            "expirationDate": "2019-12",
            "securityCode": 569,
            "name": "Fulano de Tal 1"
        },
        "packageSet": 99,
        "products": [
            99,
            98,
            95,
            91
        ],
        "id": 99,
        "userImg": ""
    },
    "error":[]
}
';

$dataObject = json_decode($data);

$directory = 'uploads';
$scanned_directory = array_diff(scandir($directory), array('..', '.'));

foreach ($scanned_directory as $file) {
    if (strpos($file, 'blob') === 0) {
        $dataObject->msgSaida->userImg = "api/uploads/" . $file;
        break;
    }
}

echo json_encode($dataObject);