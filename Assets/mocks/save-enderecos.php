<?php
header('Content-type:application/json');

$data = '
{
    "result":"SUCCESS",
    "msgSaida": {
        "addressData": [
            {
                "id": 1,
                "type": "20202",
                "postalCode": "32654987",
                "address": "Rua do Fulano",
                "number": 100,
                "complement": "Casa",
                "neighborhood": "Centro",
                "city": "Rio de Janeiro",
                "state": "RJ",
                "country": "Brasil"
            },
            {
                "id": 2,
                "type": "223626",
                "postalCode": "32654987",
                "address": "Rua do Fulano Comercial",
                "number": 100,
                "complement": "Casa",
                "neighborhood": "Centro",
                "city": "Rio de Janeiro",
                "state": "RJ",
                "country": "Brasil"
            }
        ],
        "id": 99
    },
    "error":[]
}
';

$dataObject = json_decode($data);

echo json_encode($dataObject);