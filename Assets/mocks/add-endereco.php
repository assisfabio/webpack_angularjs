<?php
header('Content-type:application/json');

$data = '{
    "result":"SUCCESS",
    "msgSaida":{
        "id": 1,
        "type": "20202",
        "postalCode": "22713110",
        "address": "Rua Liberdade da Fonseca",
        "number": "100",
        "complement": "Casa",
        "neighborhood": "Sé",
        "city": "São Paulo",
        "state": "SP",
        "country": "Brasil"
    },
    "error":[]
}
';

echo $data;