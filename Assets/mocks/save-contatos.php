<?php
header('Content-type:application/json');

$data = '
{
    "result":"SUCCESS",
    "msgSaida": {
        "scheduleData": [
            {
                "id": 1,
                "type": "97",
                "value": "fulano1@email.com.br"
            },
            {
                "id": 2,
                "type": "98",
                "value": "fulano1@emailcomercial.com.br"
            },
            {
                "id": 3,
                "type": "99",
                "value": 3136987532
            },
            {
                "id": 4,
                "type": "100",
                "value": 31986982156
            }
        ],
        "id": 99
    },
    "error":[]
}
';

$dataObject = json_decode($data);

echo json_encode($dataObject);