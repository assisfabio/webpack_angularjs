<?php

header('Content-Type: application/json');

$uploaddir = 'uploads/'; //physical address of uploads directory

$files = glob($uploaddir.'*'); // get all file names
if ($files) {
  foreach($files as $file){ // iterate files
    if(is_file($file)) {
      unlink($file); // delete file
    }
  }
}

$uploadfile = $uploaddir . basename($_FILES['foto']['name']) . time();
$data = [];

if(move_uploaded_file($_FILES['foto']['tmp_name'], $uploadfile)){
    $data['msg'] = "File was successfully uploaded.";
    $data["url"] = 'api/' . $uploadfile;
    /* Your file is uploaded into your server and you can do what ever you want with */
}else{
  $data["error"] = "Possible file upload attack!\n";
}

echo json_encode($data);
?>
