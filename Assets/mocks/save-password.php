<?php
header('Content-type:application/json');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$data = '
{
    "result":"SUCCESS",
    "msgSaida": {},
    "error":[]
}
';

$dataObject = json_decode($data);

echo json_encode($dataObject);