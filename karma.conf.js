// Karma configuration
// Generated on Fri Feb 01 2019 13:13:24 GMT-0200 (GMT-02:00)
const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WriteFilePlugin = require('write-file-webpack-plugin');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      "node_modules/jquery/dist/jquery.js",
      { 
        pattern: 'test-context.js',
        watched: false
      }
    ],

    proxies: {
      // required for component assests fetched by Angular's compiler
    },

    // list of files / patterns to exclude
    exclude: [],

    // coverage reporter generates the coverage
    reporters: ['progress', 'coverage'],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'test-context.js': ['webpack'],
      // source files, that you wanna generate coverage for
      // do not include tests or libraries
      // (these files will be instrumented by Istanbul)
      'src/**/*.js': ['coverage']
    },

    // optionally, configure the reporter
    coverageReporter: {
      includeAllSources: true,
      dir: 'coverage/',
      reporters: [
        { type: "html", subdir: "html" },
        { type: 'text-summary' }
      ]
    },
    /*coverageReporter: {
      type: 'lcov',
      dir: 'coverage/'
    },*/
    colors: true,
    

    webpack: {
      module: {
        loaders: [
          // babel
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: [
              {
                loader: "babel-loader"
              }
            ]
          },

          // html
          {
            test: /\.html$/,
            loader: "raw-loader",
            exclude: path.resolve("./src/index.html")
          },
          // Files
          {
            test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
            loader: "file-loader"
          },
          {
            test: /\.scss$/,
            use: [
              {
                loader: "style-loader"
              },
              {
                loader: "css-loader",
                options: {
                  sourceMap: false
                }
              },
              {
                loader: 'postcss-loader',
                options: {
                  plugins: function () {
                    return [
                      require('precss'),
                      require('autoprefixer')
                    ];
                  }
                }
              },
              {
                loader: "sass-loader",
                options: {
                  sourceMap: false
                }
              }
            ]
          },
          {
            test: /\.css$/,

            use: [
              {
                loader: "style-loader",
                options: {
                  sourceMap: false
                }
              },
              {
                loader: "css-loader",
                options: {
                  sourceMap: false
                }
              },
              {
                loader: "file-loader"
              }
            ]
          }
        ]
      },
      plugins: [
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery',
          'window.jQuery': 'jquery'
        }),
        new HtmlWebpackPlugin({
          filename: "index.html",
          template: path.resolve("./src/index.html")
        }),
        new WriteFilePlugin(),
        new ServiceWorkerWebpackPlugin({
          entry: path.join(__dirname, 'Assets/sw.js'),
          excludes: ['**/.*', '**/*.map', '*.html'],
          publicPath: './'
        })
      ],
      watch: true
    },
    webpackServer: {
      noInfo: true
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    // reporters: ['progress', 'coverage'],

    // web server port
    port: Math.floor(Math.ceil(Math.random() * (32000 - 9100))) + 9100,
    listenAddress: '127.0.0.1',
    hostname: 'localhost',


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],
    
    // on disconnect, makes karma to launch another phantonJs window to restart the testcases
    browserDisconnectTolerance: 5,

    // these settings help reduce the timeouts to begin with.
    browserNoActivityTimeout: 60000,
    browserDisconnectTimeout: 30000,
    captureTimeout: 60000,

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
